package net.lulihu.mule.tccSpringcloud;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.ObjectKit;
import net.lulihu.ObjectKit.KryoKit;
import net.lulihu.mule.tccTransaction.kit.ObjectContextHolder;
import net.lulihu.mule.tccTransaction.service.TransactionHandlerService;
import net.lulihu.mule.tccTransaction.service.TransactionMethodProxyService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

@Slf4j
@Aspect
@Component
public class MuleTccTransactionAspectJ implements Ordered {

    @Autowired
    private TransactionMethodProxyService methodProxyService;


    @Around("@annotation(net.lulihu.mule.tccTransaction.annotation.MuleTcc)")
    public Object interceptTccTransactionMethod(ProceedingJoinPoint point) throws Throwable {
        Object target = point.getTarget();
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        // 使用序列化的方式实现深拷贝
        // 因为参数为引用传递如果在方法内部修改该参数值可能导致后续参数不对应
        Object[] args = KryoKit.clone(point.getArgs());

        // 如果是代理对象
        if (Proxy.isProxyClass(target.getClass())) return point.proceed();

        TransactionHandlerService handlerService = methodProxyService.beforeTransactionHandler();
        //获取第一个类型方法全匹配的对象
        Class<?> firstExists = ObjectContextHolder.getInstance().chooseFirstExists(target, method);
        boolean proceed = methodProxyService.before(handlerService, firstExists, method, args);
        try {
            if (proceed) {
                Object result = point.proceed();
                return methodProxyService.result(handlerService, result);
            }
        } catch (Exception e) {
            methodProxyService.exception(handlerService, e);
        } finally {
            methodProxyService.after(handlerService);
        }
        return ObjectKit.getDefaultValue(method.getReturnType());
    }


    @Override
    public int getOrder() {
        return Integer.MAX_VALUE;
    }
}
