package net.lulihu.mule.tccSpringcloud;


import net.lulihu.mule.tccTransaction.MuleTccTransactionBootApplication;
import net.lulihu.mule.tccTransaction.service.MuleTccBootService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

/**
 * Mule Tcc SpringCloud 启动应用程序
 */
public class MuleTccSpringCloudBootApplication implements ApplicationRunner {

    @Autowired
    private MuleTccBootService muleTccBootService;

    @Autowired
    private MuleTccConfigProperties muleTccConfigProperties;

    @Override
    public void run(ApplicationArguments args) {
        new MuleTccTransactionBootApplication(muleTccBootService, muleTccConfigProperties).start();
    }

}
