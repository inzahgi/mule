package net.lulihu.disruptorKit;

import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.WorkHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * 消费者 分两种方式 重复消费/不重复消费
 * <p>
 * Disruptor.handleEventsWith 为 重复消费
 * Disruptor.handleEventsWithWorkerPool 为 不重复消费
 */
@Slf4j
public abstract class Consumer<T> implements EventHandler<Event<T>>, WorkHandler<Event<T>> {

    private final String name;

    public Consumer(String name) {
        this.name = name;
    }

    /**
     * 不重复消费执行
     *
     * @param event 事件传值
     */
    @Override
    public final void onEvent(Event<T> event) throws Exception {
        run(event);
    }

    /**
     * 重复消费执行
     *
     * @param event 事件传值
     */
    @Override
    public final void onEvent(Event<T> event, long sequence, boolean endOfBatch) throws Exception {
        run(event);
    }

    /**
     * 执行事件
     *
     * @param event 事件属性
     */
    private void run(Event<T> event) {
        if (!runBefore(event)) return;
        try {
            if (log.isDebugEnabled())
                log.debug("- {} 开始处理事件...", name);
            // 执行方法
            consumption(event);
            if (log.isDebugEnabled())
                log.debug("- {} 事件处理结束...", name);
            // 执行成功
            runSuccess(event);
        } catch (Throwable e) {
            log.error("- {} 处理事件发生错误,错误信息为:", name, e);
            // 执行错误
            runError(event, e);
        } finally {
            // 执行finally块
            runFinally(event);
        }
    }

    /**
     * 事件执行前
     *
     * @param event 事件属性
     */
    private boolean runBefore(Event<T> event) {
        boolean boo;
        try {
            if (log.isDebugEnabled())
                log.debug("- {} 开始处理事件Before块...", name);

            boo = consumptionBefore(event);

            if (log.isDebugEnabled())
                log.debug("- {} 事件Before块处理结束...", name);
        } catch (Exception e) {
            log.error("- {} 处理事件Before块发生错误,错误信息为:", name, e);
            boo = false;
        }
        return boo;
    }


    /**
     * 事件执行成功
     *
     * @param event 事件属性
     */
    private void runSuccess(Event<T> event) {
        try {
            if (log.isDebugEnabled())
                log.debug("- {} 开始处理事件Success块...", name);

            consumptionSuccess(event);

            if (log.isDebugEnabled())
                log.debug("- {} 事件Success块处理结束...", name);
        } catch (Exception e) {
            log.error("- {} 处理事件Success块发生错误,错误信息为:", name, e);
        }
    }

    /**
     * 执行事件错误
     *
     * @param event     事件属性
     * @param throwable 错误信息
     */
    private void runError(Event<T> event, Throwable throwable) {
        try {
            if (log.isDebugEnabled())
                log.debug("- {} 开始处理事件Error块...", name);

            consumptionError(event, throwable);

            if (log.isDebugEnabled())
                log.debug("- {} 事件Error块处理结束...", name);
        } catch (Exception e) {
            log.error("- {} 处理事件Error块发生错误,错误信息为:", name, e);
        }
    }

    /**
     * 执行事件Finally块
     *
     * @param event 事件属性
     */
    private void runFinally(Event<T> event) {
        try {
            if (log.isDebugEnabled())
                log.debug("- {} 开始处理事件Finally块...", name);

            consumptionFinally(event);

            if (log.isDebugEnabled())
                log.debug("- {} 事件Finally块处理结束...", name);
        } catch (Throwable e) {
            log.error("- {} 处理事件Finally块发生错误,错误信息为:", name, e);
        }
    }

    /**
     * 消费方法实现
     *
     * @param event 事件传值
     */
    public abstract void consumption(Event<T> event) throws Exception;


    /**
     * 事件执行前回调
     *
     * @param event
     * @return 如果返回true则继续执行事件，反之跳过
     * @throws Exception 一旦发生异常将导致不执行事件
     */
    public boolean consumptionBefore(Event<T> event) throws Exception {
        return true;
    }

    /**
     * 消费成功回调
     *
     * @param event
     */
    public void consumptionSuccess(Event<T> event) throws Exception {
    }

    /**
     * 消费时发生异常信息处理回调
     *
     * @param event 事件传值
     */
    public void consumptionError(Event<T> event, Throwable e) throws Exception {
    }

    /**
     * 消费方法实现方法的finally块回调
     *
     * @param event 事件传值
     */
    public void consumptionFinally(Event<T> event) throws Exception {
    }


}
