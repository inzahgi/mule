package net.lulihu.disruptorKit;

/**
 * 事件对象执行结束时进行内部数据清除，方便gc
 */
public interface EventEndClear {

    void clear();

}
