package net.lulihu.disruptorKit.demo;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import net.lulihu.disruptorKit.*;

public class HelloWorld {

    public static void main(String[] args) {
        DisruptorManage instance = DisruptorManage.getInstance();
        EventFactory<Event<TestEvent>> eventFactory = DefaultEventFactory.factory();
        //注册
        Disruptor<Event<TestEvent>> eventDisruptor =
                instance.registered("test", eventFactory, 16,
                        ProducerType.MULTI, new BlockingWaitStrategy());
        Producer<TestEvent> producer = new Producer<>(eventDisruptor.getRingBuffer());
        // 定义消费者  不重复消费
        eventDisruptor
                .handleEventsWithWorkerPool(new Consumer1(), new Consumer3())
                .handleEventsWithWorkerPool(new Consumer2());

        // 启动
        eventDisruptor.start();


        for (int i = 0; i < 2; i++) {
            TestEvent testEvent = new TestEvent(i);
            producer.submit(testEvent);
        }
    }

    @Data
    @AllArgsConstructor
    @ToString
    public static class TestEvent {
        private Integer i;
    }

    public static class Consumer1 extends Consumer<TestEvent> {

        public Consumer1() {
            super("消费者1");
        }

        @Override
        public void consumption(Event<TestEvent> event) throws Exception {
            System.out.println(event.getElement().toString());
        }
    }

    public static class Consumer2 extends Consumer<TestEvent> {

        public Consumer2() {
            super("消费者2");
        }

        @Override
        public void consumption(Event<TestEvent> event) throws Exception {
            System.out.println(event.getElement().toString());
        }
    }

    public static class Consumer3 extends Consumer<TestEvent> {

        public Consumer3() {
            super("消费者3");
        }

        @Override
        public void consumption(Event<TestEvent> event) throws Exception {
            System.out.println(event.getElement().toString());
        }
    }
}
