package net.lulihu.disruptorKit;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 定义事件
 * 事件(Event)就是通过 Disruptor 进行交换的数据类型
 */
@ToString
public class Event<T> {

    /**
     * 生产的元件
     */
    @Getter
    @Setter
    private T element;


    /**
     * 清除引用，方便GC
     */
    public void clear() {
        if (this.element instanceof EventEndClear) ((EventEndClear) this.element).clear();
        this.element = null;
    }

}
