package net.lulihu.disruptorKit;

import com.alibaba.fastjson.JSON;
import com.lmax.disruptor.RingBuffer;
import lombok.extern.slf4j.Slf4j;
import net.lulihu.Assert;

/**
 * 生产者
 */
@Slf4j
public class Producer<T> {

    private final RingBuffer<Event<T>> ringBuffer;

    public Producer(RingBuffer<Event<T>> ringBuffer) {
        Assert.notNull(ringBuffer, "RingBuffer 不可以为空");
        this.ringBuffer = ringBuffer;
    }

    /**
     * 提交元件，供消费者消费
     */
    public void submit(T element) {
        if (log.isTraceEnabled())
            log.trace("添加生产信息: {}", JSON.toJSONString(element));

        //请求下一个事件序号
        long sequence = ringBuffer.next();
        try {
            //填写数据
            Event<T> tEvent = ringBuffer.get(sequence);
            tEvent.setElement(element);
        } finally {
            //发布
            ringBuffer.publish(sequence);
        }
    }


}
