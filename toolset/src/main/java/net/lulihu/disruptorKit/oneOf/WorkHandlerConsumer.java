package net.lulihu.disruptorKit.oneOf;

import com.lmax.disruptor.WorkHandler;
import net.lulihu.designPattern.chain.ResponsibilityChainHandler;
import net.lulihu.disruptorKit.Event;

/**
 * 不重复消费模式
 */
public class WorkHandlerConsumer implements WorkHandler<Event<Object>> {

    private final WorkHandlerManage consumer;

    public WorkHandlerConsumer() {
        consumer = WorkHandlerManage.getInstance();
    }

    @Override
    public void onEvent(Event<Object> event) throws Exception {
        Object element = event.getElement();
        // 获取责任链
        ResponsibilityChainHandler workHandler = consumer.getWorkHandler(element.getClass());
        // 执行处理
        workHandler.handler(element);
    }
}
