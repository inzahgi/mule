package net.lulihu.ObjectKit;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.http.WafRequestWrapper;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Http 工具类 ，不包含发送 Http请求
 */
@Slf4j
public class HttpKit {

    /**
     * 获取非请求流参数
     *
     * @param request 请求对象
     */
    public static Map<String, String> getRequestParameters(HttpServletRequest request) {
        return getRequestParameters(new TreeMap<>(Comparator.naturalOrder()), request);
    }

    public static Map<String, String> getRequestParameters(Map<String, String> result, HttpServletRequest request) {
        if (!(request instanceof WafRequestWrapper)) request = new WafRequestWrapper(request);

        Enumeration<String> enums = request.getParameterNames();
        while (enums.hasMoreElements()) {
            String paramName = enums.nextElement();
            result.put(paramName, request.getParameter(paramName));
        }
        return result;
    }

    /**
     * 获取非请求流的请求参数
     */
    public static Map<String, Object> getRequestParameters(HttpServletRequest request, Map<String, Object> result) {
        if (!(request instanceof WafRequestWrapper)) request = new WafRequestWrapper(request);

        Enumeration<String> enums = request.getParameterNames();
        while (enums.hasMoreElements()) {
            String paramName = enums.nextElement();
            result.put(paramName, request.getParameter(paramName));
        }
        return result;
    }

    public static Map<String, Object> getHeaderParam(HttpServletRequest request) {
        if (!(request instanceof WafRequestWrapper)) request = new WafRequestWrapper(request);

        Map<String, Object> result = new HashMap<>();
        Enumeration<String> headerNames = request.getHeaderNames();
        for (; headerNames.hasMoreElements(); ) {
            String paramName = headerNames.nextElement();
            String paramValue = request.getHeader(paramName);
            result.put(paramName, paramValue);
        }
        return result;
    }

    /**
     * 获取指定请求头
     *
     * @param request 请求对象
     * @param key     键
     * @return 根据键获取指定的值
     */
    public static String getHeader(HttpServletRequest request, String key) {
        return getRequest(request).getHeader(key);
    }

    /**
     * 获取 包装防Xss Sql注入的 HttpServletRequest
     *
     * @return request
     */
    public static HttpServletRequest getRequest(HttpServletRequest request) {
        return new WafRequestWrapper(request);
    }

    /**
     * 获取客户真实的ip地址
     *
     * @param request 请求对象
     */
    public static String getClientIP(HttpServletRequest request) {
        if (!(request instanceof WafRequestWrapper)) request = new WafRequestWrapper(request);

        String ip = request.getHeader("x-forwarded-for");
        if (StrKit.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (StrKit.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (StrKit.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

}
