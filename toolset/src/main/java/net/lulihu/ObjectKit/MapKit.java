package net.lulihu.ObjectKit;

import com.alibaba.fastjson.JSON;
import net.lulihu.Assert;
import net.lulihu.functional.MapGroup;

import java.lang.reflect.Field;
import java.util.*;

/**
 * map工具类
 */
public class MapKit {

    private MapKit() {
    }

    /**
     * map为javaBean对象赋值
     * <p>
     * 注意：map的key必须为指定对象的属性名称 支持驼峰 默认为下划线
     *
     * @param map map
     * @param t   赋值对象
     */
    public static <T> T toJavaBean(Map<String, Object> map, T t) {
        return toJavaBean(map, t, StrKit.UNDERLINE);
    }

    /**
     * map为javaBean对象赋值
     * <p>
     * 注意：map的key必须为指定对象的属性名称 支持驼峰
     *
     * @param map       map
     * @param t         赋值对象
     * @param underline 驼峰转换时的连接符 长度不可以大于1 否则会导致匹配不到
     */
    public static <T> T toJavaBean(Map<String, Object> map, T t, String underline) {
        if (isNotEmpty(map)) {
            for (Field field : ReflectKit.getAllFieldsList(t.getClass())) {
                String fieldName = field.getName();
                Object v;
                if (map.containsKey(fieldName)) v = map.get(fieldName);
                else v = map.get(StrKit.toUnderlineCase(fieldName, underline));

                if (ObjectKit.hasNotEmpty(v)) {
                    ReflectKit.setFieldValue(t, fieldName, v);
                }
            }
        }
        return t;
    }

    /**
     * 对象转TreeMap
     *
     * @param obj        对象
     * @param comparator 排序比较器
     * @throws IllegalAccessException 反射获取属性值错误，抛出异常
     */
    public static TreeMap<String, String> toTreeMap(Object obj, Comparator<String> comparator)
            throws IllegalAccessException {
        TreeMap<String, String> result = null;
        if (BeanKit.isBean(obj.getClass()))
            result = javaBeanToTreeMap(obj, comparator);
        if (obj instanceof Map) {
            result = new TreeMap<>(comparator);
            result.putAll((Map<String, String>) obj);
        }
        return result;
    }


    /**
     * javaBean对象转TreeMap
     *
     * @param obj        对象
     * @param comparator 排序比较器
     * @throws IllegalAccessException 反射获取属性值错误，抛出异常
     */
    public static TreeMap<String, String> javaBeanToTreeMap(Object obj, Comparator<String> comparator)
            throws IllegalAccessException {
        Assert.isTrue(!BeanKit.isBean(obj.getClass()), "转换对象必须为javaBean");

        TreeMap<String, String> treeMap = new TreeMap<>(comparator);
        for (Field field : ReflectKit.getAllFieldsList(obj.getClass())) {
            String fieldName = field.getName();
            Object value = ReflectKit.getFieldValue(obj, fieldName);
            if (value != null) { // 空值不保存
                // 不是基础数据类型 转json
                if (!BeanKit.isPrimitive(value.getClass()))
                    value = JSON.toJSONString(value);
                treeMap.put(fieldName, value.toString());
            }
        }
        return treeMap;
    }

    /**
     * 将Entry集合转换为HashMap
     *
     * @param entryCollection entry集合
     * @return Map
     */
    public static <T, K> HashMap<T, K> toMap(Collection<Map.Entry<T, K>> entryCollection) {
        HashMap<T, K> map = new HashMap<>();
        for (Map.Entry<T, K> entry : entryCollection) {
            map.put(entry.getKey(), entry.getValue());
        }
        return map;
    }

    /**
     * 集合对象转HashMap
     *
     * @param list     集合对象
     * @param mapGroup 分组方法
     * @param <O>      泛型
     */
    public static <O, V> Map<String, V> listToHashMap(List<O> list, MapGroup<O, String, V> mapGroup) {
        return listToMap(list, new HashMap<>(), mapGroup);
    }

    /**
     * * 集合对象转Map
     *
     * @param list     集合对象
     * @param result   封装结果
     * @param mapGroup 分组方法
     * @param <O>      泛型
     */
    public static <O, K, V> Map<K, V> listToMap(List<O> list, Map<K, V> result, MapGroup<O, K, V> mapGroup) {
        Assert.notNull(result, "结果封装map不可以为空");

        if (CollectionKit.isEmpty(list)) return result;

        for (O obj : list) {
            mapGroup.accept(obj, result);
        }
        return result;
    }

    /**
     * Map是否为空
     *
     * @param map 集合
     * @return 是否为空
     */

    public static boolean isEmpty(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    /**
     * Map是否为非空
     *
     * @param map 集合
     * @return 是否为非空
     */
    public static <T> boolean isNotEmpty(Map<?, ?> map) {
        return !isEmpty(map);
    }

    /**
     * 新建一个HashMap
     *
     * @return HashMap对象
     */
    public static <T, K> HashMap<T, K> newHashMap() {
        return new HashMap<>();
    }

    /**
     * 新建一个HashMap
     *
     * @param size 初始大小，由于默认负载因子0.75，传入的size会实际初始大小为size / 0.75
     * @return HashMap对象
     */
    public static <T, K> HashMap<T, K> newHashMap(int size) {
        return new HashMap<>((int) (size / 0.75));
    }

    /**
     * map的key转为小写
     *
     * @param map 被转换的集
     * @return Map<String , Object> 转换后新的集
     */
    public static Map<String, Object> caseInsensitiveMap(Map<String, Object> map) {
        Map<String, Object> tempMap = new HashMap<>();
        for (String key : map.keySet()) {
            tempMap.put(key.toLowerCase(), map.get(key));
        }
        return tempMap;
    }


    /**
     * 获取map中第一个数据值
     *
     * @param <K> Key的类型
     * @param <V> Value的类型
     * @param map 数据源
     * @return 返回的值
     */
    public static <K, V> V getFirstOrNull(Map<K, V> map) {
        V obj = null;
        for (Map.Entry<K, V> entry : map.entrySet()) {
            obj = entry.getValue();
            if (obj != null) {
                break;
            }
        }
        return obj;
    }

}
