package net.lulihu.ObjectKit;


import net.lulihu.ObjectKit.annotation.Prop;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

/**
 * Properties 对象工具类
 */
public class PropertiesKit {

    private PropertiesKit() {
    }

    /**
     * bean转 Properties对象
     *
     * @param bean 需要转换的对象
     * @param <T>
     * @throws IllegalAccessException
     */
    public static <T> Properties beanToProperties(T bean) throws IllegalAccessException {
        Objects.requireNonNull(bean);

        Properties result = new Properties();
        List<Field> fields = ReflectKit.getAllFieldsList(bean.getClass());

        String name = null;
        try {
            for (Field field : fields) {
                Prop prop = field.getAnnotation(Prop.class);
                if (prop == null) continue;

                name = field.getName();
                Object value = ReflectKit.getFieldValue(bean, name);

                result.put(prop.value(), value);
            }
        } catch (IllegalAccessException e) {
            throw new IllegalAccessException(StrKit.format("非法访问属性[{}]例外", name));
        }
        return result;
    }

}
