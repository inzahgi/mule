package net.lulihu.ObjectKit;

import net.lulihu.functional.ConsumptionResult;
import org.slf4j.Logger;

/**
 * 日志工具 仅是对{@link Logger}对象的一层封装
 */
public class LogKit {

    public static void debug(Logger log, String msg) {
        if (log.isDebugEnabled()) log.debug(msg);
    }

    public static void debug(Logger log, String format, Object arg) {
        if (log.isDebugEnabled()) log.debug(format, arg);
    }

    public static void debug(Logger log, String format, Object arg1, Object arg2) {
        if (log.isDebugEnabled()) log.debug(format, arg1, arg2);
    }

    public static void debug(Logger log, String format, Object... arguments) {
        if (log.isDebugEnabled()) log.debug(format, arguments);
    }

    public static void debug(Logger log, String msg, Throwable t) {
        if (log.isDebugEnabled()) log.debug(msg, t);
    }

    public static void debug(Logger log, String format, ConsumptionResult<Object[]> consumption) {
        if (log.isDebugEnabled()) log.debug(format, consumption.accept());
    }

    /*------------------------------------ info --------------------------------------------------------*/

    public static void info(Logger log, String msg) {
        if (log.isInfoEnabled()) log.info(msg);
    }

    public static void info(Logger log, String format, Object arg) {
        if (log.isInfoEnabled()) log.info(format, arg);
    }

    public static void info(Logger log, String format, Object arg1, Object arg2) {
        if (log.isInfoEnabled()) log.info(format, arg1, arg2);
    }

    public static void info(Logger log, String format, Object... arguments) {
        if (log.isInfoEnabled()) log.info(format, arguments);
    }

    public static void info(Logger log, String msg, Throwable t) {
        if (log.isInfoEnabled()) log.info(msg, t);
    }

    public static void info(Logger log, String format, ConsumptionResult<Object[]> consumption) {
        if (log.isInfoEnabled()) log.info(format, consumption.accept());
    }

    /*------------------------------------ warn --------------------------------------------------------*/

    public static void warn(Logger log, String msg) {
        if (log.isWarnEnabled()) log.warn(msg);
    }

    public static void warn(Logger log, String format, Object arg) {
        if (log.isWarnEnabled()) log.warn(format, arg);
    }

    public static void warn(Logger log, String format, Object arg1, Object arg2) {
        if (log.isWarnEnabled()) log.warn(format, arg1, arg2);
    }

    public static void warn(Logger log, String format, Object... arguments) {
        if (log.isWarnEnabled()) log.warn(format, arguments);
    }

    public static void warn(Logger log, String msg, Throwable t) {
        if (log.isWarnEnabled()) log.warn(msg, t);
    }

    public static void warn(Logger log, String format, ConsumptionResult<Object[]> consumption) {
        if (log.isWarnEnabled()) log.warn(format, consumption.accept());
    }

    /*------------------------------------ error --------------------------------------------------------*/

    public static void error(Logger log, String msg) {
        if (log.isErrorEnabled()) log.error(msg);
    }

    public static void error(Logger log, String format, Object arg) {
        if (log.isErrorEnabled()) log.error(format, arg);
    }

    public static void error(Logger log, String format, Object arg1, Object arg2) {
        if (log.isErrorEnabled()) log.error(format, arg1, arg2);
    }

    public static void error(Logger log, String format, Object... arguments) {
        if (log.isErrorEnabled()) log.error(format, arguments);
    }

    public static void error(Logger log, String msg, Throwable t) {
        if (log.isErrorEnabled()) log.error(msg, t);
    }

    public static void error(Logger log, String format, ConsumptionResult<Object[]> consumption) {
        if (log.isErrorEnabled()) log.error(format, consumption.accept());
    }
}
