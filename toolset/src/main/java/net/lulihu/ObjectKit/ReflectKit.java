package net.lulihu.ObjectKit;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 反射工具类
 */
@Slf4j
public class ReflectKit {

    /**
     * 获取对象所有的属性
     *
     * @param cls 指定对象
     * @return 所有的属性
     */
    public static List<Field> getAllFieldsList(Class<?> cls) {
        return FieldUtils.getAllFieldsList(cls);
    }

    /**
     * 获取属性值
     *
     * @param obj       对象
     * @param fieldName 对象属性名
     * @return 对象属性值
     */
    public static Object getFieldValue(Object obj, String fieldName) throws IllegalAccessException {
        if (obj == null) {
            return null;
        }
        Field targetField = getTargetField(obj.getClass(), fieldName);
        return FieldUtils.readField(targetField, obj, true);
    }

    /**
     * 获取目标属性
     *
     * @param targetClass 指定对象
     * @param fieldName   属性名称
     * @return 属性封装对象
     */
    public static Field getTargetField(Class<?> targetClass, String fieldName) {
        if (targetClass == null || Object.class.equals(targetClass)) return null;

        Field field;
        field = FieldUtils.getDeclaredField(targetClass, fieldName, true);
        if (field == null) {
            field = getTargetField(targetClass.getSuperclass(), fieldName);
        }
        return field;
    }

    /**
     * 设置属性值
     *
     * @param obj       对象
     * @param fieldName 属性名称
     * @param value     设置的值
     */
    public static void setFieldValue(Object obj, String fieldName, Object value) {
        if (null == obj) {
            return;
        }
        Field targetField = getTargetField(obj.getClass(), fieldName);
        try {
            FieldUtils.writeField(targetField, obj, value);
        } catch (IllegalAccessException e) {
            LogKit.error(log, "给[{}]的属性[{}]设置值时发生例外", obj, fieldName, e);
        }
    }

    /**
     * 对象转map
     */
    public static <T> Map<String, String> setMapValues(T from, Map<String, String> map) throws Exception {
        // 父类参数封装
        Class<?> parentClazz = from.getClass().getSuperclass();
        buildMap(from, parentClazz, map);
        // 子类参数封装
        Class<?> clazzFrom = from.getClass();
        buildMap(from, clazzFrom, map);
        return map;
    }

    /**
     * 构建map值
     */
    public static <T> void buildMap(T from, Class<?> clazz, Map<String, String> map) throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        Field[] toField = clazz.getDeclaredFields();
        for (Field field : toField) {
            String fieldName = field.getName();
            PropertyDescriptor pd = new PropertyDescriptor(fieldName, clazz);
            Method getMethod = pd.getReadMethod();
            Object o = getMethod.invoke(from);
            if (ObjectKit.hasNotEmpty(o)) {
                String value;
                if (o instanceof Collection || o instanceof Map)
                    value = JSON.toJSONString(o);
                else
                    value = o.toString();

                map.put(fieldName, value);
            }
        }
    }
}