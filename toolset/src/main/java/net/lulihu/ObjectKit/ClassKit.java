package net.lulihu.ObjectKit;


import net.lulihu.exception.ToolBoxException;

import java.lang.reflect.*;

/**
 * 类工具类
 */
@SuppressWarnings("unchecked")
public class ClassKit {

    private ClassKit() {
        // 静态类不可实例化
    }

    /**
     * 获取泛型类Class对象，不是泛型类则返回null
     *
     * @param clazz 泛型类
     * @return 如果是泛型类返回泛型类，反之为null
     */
    public static Class<?>[] getActualTypeArgument(Class<?> clazz) {
        Class<?>[] resultClass = null;
        Type genericSuperclass = clazz.getGenericSuperclass();
        if (genericSuperclass instanceof ParameterizedType) {
            Type[] actualTypeArguments = ((ParameterizedType) genericSuperclass).getActualTypeArguments();
            if (CollectionKit.isEmpty(actualTypeArguments)) {
                int length = actualTypeArguments.length;

                resultClass = new Class[length];
                for (int i = 0; i < length; i++) {
                    resultClass[i] = (Class<?>) actualTypeArguments[i];
                }
            }
        }
        return resultClass;
    }

    /**
     * 是否为标准的类<br>
     * 这个类必须：<br>
     * 1、非接口 2、非抽象类 3、非Enum枚举 4、非数组 5、非注解 6、非原始类型（int, long等）
     *
     * @param clazz 类
     * @return 是否为标准类
     */
    public static boolean isNormalClass(Class<?> clazz) {
        return null != clazz && !clazz.isInterface() && !isAbstract(clazz) && !clazz.isEnum() && !clazz.isArray() && !clazz.isAnnotation()
                && !clazz.isSynthetic() && !clazz.isPrimitive();
    }

    /**
     * 是否为抽象类
     *
     * @param clazz 类
     * @return 是否为抽象类
     */
    public static boolean isAbstract(Class<?> clazz) {
        return Modifier.isAbstract(clazz.getModifiers());
    }

    /**
     * 实例化对象
     *
     * @param clazz 类名
     * @return 对象
     */
    @SuppressWarnings("unchecked")
    public static <T> T newInstance(String clazz) {
        if (null == clazz)
            return null;
        try {
            return (T) Class.forName(clazz).newInstance();
        } catch (Exception e) {
            throw new ToolBoxException(StrKit.format("实例类 [{}] 错误!!", clazz), e);
        }
    }

    /**
     * 根据实例类型创建一个新的实例，优先使用无参构造函数，如果不存在无参构造函数则为第一个构造函数的参数设置默认值
     *
     * @param clazz 实例类型
     * @return 新的实例
     */
    public static <T> T newInstanceConstructorsDefaultValue(Class<T> clazz) {
        if (null == clazz) return null;

        // 优先使用无参构造函数
        Constructor<T> constructor = priorityNoArgumentConstructor(clazz);
        Class[] parameterTypes = constructor.getParameterTypes();

        if (CollectionKit.isEmpty(parameterTypes)) return newInstance(clazz);

        int length = parameterTypes.length;
        Object[] args = new Object[length];
        for (int i = 0; i < length; i++) {
            Class cla = parameterTypes[i];
            if (BeanKit.isPrimitive(cla)) {
                args[i] = ObjectKit.primitiveDefaultValue(cla);
            } else {
                args[i] = null;
            }
        }

        try {
            return constructor.newInstance(args);
        } catch (Exception e) {
            throw new ToolBoxException(StrKit.format("实例类 [{}] 错误!", clazz), e);
        }
    }


    /**
     * 优先获取对象的无参构造函数如果不存在则返回第一个构造函数
     *
     * @param clazz 对象类型
     * @return 对象的构造函数
     */
    public static <T> Constructor<T> priorityNoArgumentConstructor(Class<T> clazz) {
        Constructor[] constructors = clazz.getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            Class[] parameterTypes = constructor.getParameterTypes();
            if (CollectionKit.isEmpty(parameterTypes)) {
                return constructor;
            }
        }
        return constructors[0];
    }

    /**
     * 实例化对象
     *
     * @param clazz 类
     * @return 对象
     */
    public static <T> T newInstance(Class<T> clazz) {
        if (null == clazz) return null;

        try {
            return clazz.newInstance();
        } catch (Exception e) {
            throw new ToolBoxException(StrKit.format("实例类 [{}] 错误!", clazz), e);
        }
    }

    /**
     * 实例化对象
     *
     * @param clazz 类
     * @return 对象
     */
    public static <T> T newInstance(Class<T> clazz, Object... params) {
        if (null == clazz)
            return null;
        if (CollectionKit.isEmpty(params)) {
            return newInstance(clazz);
        }
        try {
            return clazz.getDeclaredConstructor(getClasses(params)).newInstance(params);
        } catch (Exception e) {
            throw new ToolBoxException(StrKit.format("实例类 [{}] 发生例外!", clazz), e);
        }
    }

    /**
     * 获得对象数组的类数组
     *
     * @param objects 对象数组
     * @return 类数组
     */
    public static Class<?>[] getClasses(Object... objects) {
        Class<?>[] classes = new Class<?>[objects.length];
        for (int i = 0; i < objects.length; i++) {
            classes[i] = objects[i].getClass();
        }
        return classes;
    }

    /**
     * 检查目标类是否可以从原类转化<br>
     * 转化包括：<br>
     * 1、原类是对象，目标类型是原类型实现的接口<br>
     * 2、目标类型是原类型的父类<br>
     * 3、两者是原始类型或者包装类型（相互转换）
     *
     * @param targetType 目标类型
     * @param sourceType 原类型
     * @return 是否可转化
     */
    public static boolean isAssignable(Class<?> targetType, Class<?> sourceType) {
        if (null == targetType || null == sourceType) {
            return false;
        }

        // 对象类型
        if (targetType.isAssignableFrom(sourceType)) {
            return true;
        }

        // 基本类型
        if (targetType.isPrimitive()) {
            // 原始类型
            Class<?> resolvedPrimitive = BasicType.wrapperPrimitiveMap.get(sourceType);
            return targetType == resolvedPrimitive;
        } else {
            // 包装类型
            Class<?> resolvedWrapper = BasicType.primitiveWrapperMap.get(sourceType);
            return resolvedWrapper != null && targetType.isAssignableFrom(resolvedWrapper);
        }
    }

    /**
     * 设置方法为可访问
     *
     * @param method 方法
     * @return 方法
     */
    public static Method setAccessible(Method method) {
        if (null != method && ClassKit.isNotPublic(method)) {
            method.setAccessible(true);
        }
        return method;
    }

    /**
     * 指定类是否为非public
     *
     * @param clazz 类
     * @return 是否为非public
     */
    public static boolean isNotPublic(Class<?> clazz) {
        return !isPublic(clazz);
    }

    /**
     * 指定方法是否为非public
     *
     * @param method 方法
     * @return 是否为非public
     */
    public static boolean isNotPublic(Method method) {
        return !isPublic(method);
    }

    /**
     * 指定类是否为Public
     *
     * @param clazz 类
     * @return 是否为public
     */
    public static boolean isPublic(Class<?> clazz) {
        if (null == clazz) {
            throw new NullPointerException("提供的类为null");
        }
        return Modifier.isPublic(clazz.getModifiers());
    }

    /**
     * 指定方法是否为Public
     *
     * @param method 方法
     * @return 是否为public
     */
    public static boolean isPublic(Method method) {
        if (null == method) {
            throw new NullPointerException("提供的方法为null");
        }
        return isPublic(method.getDeclaringClass());
    }


    /**
     * 获取对象的顶层父类（Object不算）
     *
     * @param obj 对象
     * @return 超类
     */
    public static Class<?> getSuperclass(Object obj) {
        return getSuperclass(obj.getClass());

    }

    /**
     * 获取对象的顶层父类（Object不算）
     *
     * @param clazz 对象类型
     * @return 超类
     */
    public static Class<?> getSuperclass(Class<?> clazz) {
        if (clazz == Object.class) return null;

        Class<?> superclass = clazz.getSuperclass();
        Class<?> aClass = getSuperclass(superclass);
        if (aClass == null) return clazz;
        return getSuperclass(aClass);
    }
}