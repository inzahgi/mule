package net.lulihu.functional;


@FunctionalInterface
public interface ValueProvider {
    /**
     * 获取值
     *
     * @param name  Bean对象中参数名
     * @param clazz Bean对象中参数类型
     * @return 对应参数名的值
     */
    Object value(String name, Class<?> clazz);
}