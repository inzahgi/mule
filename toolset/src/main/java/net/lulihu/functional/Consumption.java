package net.lulihu.functional;

@FunctionalInterface
public interface Consumption {

    /**
     * 接收指定表达式，等待执行
     */
    void accept();

}
