package net.lulihu.functional;

@FunctionalInterface
public interface Consumer<T> {

    /**
     * 接收指定表达式，等待执行
     */
    void accept(T t);


}
