package net.lulihu.http.okhttp;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.FileKit;
import net.lulihu.exception.RequestException;
import okhttp3.*;
import okhttp3.Request.Builder;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;
import net.lulihu.http.MIME;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 基于OKhttp3的HTTP协议请求封装
 */
@Slf4j
public class HttpUtils {

    /**
     * 连接超时时间 /秒
     */
    public static final Integer CONNECT_TIMEOUT = 10;

    /**
     * 读取超时时间 /秒
     */
    public static final Integer READ_TIMEOUT = 10;

    /**
     * 获取响应流
     *
     * @param url 请求路径
     * @throws RequestException
     */
    public static BufferedSource getResponseSource(final String url) throws RequestException {
        Request request = new Builder().url(url).build();
        try {
            Response response = sendRequest(request);
            if (!response.isSuccessful()) {
                throw new RequestException("请求路径：" + url + "异常! 错误摘要:" + response.toString());
            }
            ResponseBody body = response.body();
            return body.source();
        } catch (IOException e) {
            throw new RequestException("请求路径：" + url + "异常！:", e);
        }
    }

    /**
     * 下载小文件
     *
     * @param url      网络路径
     * @param filePath 文件保存路径
     * @return
     */
    public static boolean downloadSmallFile(final String url, final String filePath) throws RequestException {
        return downloadSmallFile(url, new File(filePath));
    }

    public static boolean downloadSmallFile(final String url, final File file) throws RequestException {
        BufferedSink sink = null;
        BufferedSource source = null;
        try {
            source = getResponseSource(url);
            sink = Okio.buffer(Okio.sink(FileKit.createFile(file)));
            sink.writeAll(source);
            sink.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (sink != null)
                try {
                    sink.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            if (source != null)
                try {
                    source.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return true;
    }

    /**
     * 请求日志
     *
     * @param request
     */
    private static void requestLog(Request request) {
        if (log.isDebugEnabled()) {
            log.debug("请求地址： " + request.url());
            log.debug("请求类型： " + request.method());
            if ("POST".equals(request.method())) {
                Map<String, String> reqParam = new LinkedHashMap<>();
                RequestBody body = request.body();
                if (body instanceof FormBody) { // 默认表单
                    FormBody formBody = (FormBody) body;
                    int size = formBody.size();
                    for (int i = 0; i < size; i++) {
                        reqParam.put(formBody.encodedName(i), formBody.encodedValue(i));
                    }
                } else if (body instanceof NoEncodeFormBody) { // 自定义表单
                    NoEncodeFormBody formBody = (NoEncodeFormBody) body;
                    int size = formBody.size();
                    for (int i = 0; i < size; i++) {
                        reqParam.put(formBody.encodedName(i), formBody.encodedValue(i));
                    }
                }
                log.debug("请求参数： " + JSON.toJSONString(reqParam));
            }
            log.debug("请求头部参数： " + request.headers());
        }
    }

    /**
     * 发送http请求
     */
    private static Response sendRequest(Request request) throws RequestException, IOException {
        // 打印日志
        requestLog(request);
        // 构建请求
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .build();
        // 响应
        long startTime = System.nanoTime();
        Response response = client.newCall(request).execute();
        if (log.isDebugEnabled())
            log.debug("请求耗时:" + (System.nanoTime() - startTime) / 1000 + " /μs");

        // 请求成功
        if (response.isSuccessful()) {
            return response;
        }
        throw new RequestException("请求错误: \n\t错误状态: {} \t错误信息: {} \t错误请求主体: {}",
                response.code(), response.message(), response.body().string());
    }

    /**
     * 返回结果封装
     */
    private static <T> T pckagingResult(Request request, Class<T> resultClazz) throws RequestException {
        try {
            Response response = sendRequest(request);
            String result = response.body().string();
            if (log.isDebugEnabled())
                log.debug("请求结果: " + result);
            return JSON.parseObject(result, resultClazz);
        } catch (IOException e) {
            throw new RequestException("发送地址为" + request.url() + "的请求错误", e);
        }
    }

    /**
     * 设置多部分post请求参数
     */
    private static void setPostParam(Map<String, String> params, MultipartBody.Builder multipartBodyBuilder) {
        if (params != null && params.size() > 0) {
            for (String key : params.keySet()) {
                multipartBodyBuilder.addFormDataPart(key, params.get(key));
            }
        }
    }

    /**
     * 设置POST请求表单参数
     */
    private static void setPostParam(Map<String, String> params, FormBody.Builder builder) {
        if (params != null && params.size() > 0) {
            for (String key : params.keySet()) {
                builder.add(key, params.get(key));
            }
        }
    }

    /**
     * 设置请求头部信息
     */
    private static void setHeadParam(Builder requestBuilder, Map<String, String> headParam) {
        if (headParam != null && headParam.size() > 0) {
            for (String key : headParam.keySet()) {
                requestBuilder.addHeader(key, headParam.get(key));
            }
        }
    }

    /**
     * 获取表单格式请求主体
     */
    private static MultipartBody.Builder getFormMultipartBody(Map<String, String> params) {
        MultipartBody.Builder multipartBodyBuilder = new MultipartBody.Builder();
        multipartBodyBuilder.setType(MultipartBody.FORM);
        setPostParam(params, multipartBodyBuilder);
        return multipartBodyBuilder;
    }


    /**
     * POST请求 自定义表单方式提交
     *
     * @param url       请求路径
     * @param headParam 头部参数
     * @param params    请求参数
     * @return
     * @throws RequestException
     */
    public static <T> T sendPOSTNoEncode(String url, Map<String, String> headParam, Map<String, String> params, Class<T> resultClazz) throws RequestException {
        // 构建请求体
        Builder requestBuilder = new Builder();
        // 构建请求主体参数
        NoEncodeFormBody body = getNoEncodeFormBody(params).build();
        // 添加请求地址
        requestBuilder.url(url);
        // 请求主体
        requestBuilder.post(body);
        // 请求头部参数
        setHeadParam(requestBuilder, headParam);
        Request request = requestBuilder.build();
        return pckagingResult(request, resultClazz);
    }

    /**
     * 获取表单格式请求主体
     */
    private static NoEncodeFormBody.Builder getNoEncodeFormBody(Map<String, String> params) {
        NoEncodeFormBody.Builder builder = new NoEncodeFormBody.Builder();
        params.forEach(builder::add);
        return builder;
    }

    /**
     * 获取表单格式请求主体
     */
    private static FormBody.Builder getFormBody(Map<String, String> params) {
        FormBody.Builder builder = new FormBody.Builder();
        setPostParam(params, builder);
        return builder;
    }

    /**
     * POST请求 表单方式提交
     *
     * @param url       请求路径
     * @param headParam 头部参数
     * @param params    请求参数
     * @return
     * @throws RequestException
     */
    public static <T> T sendPOST(String url, Map<String, String> headParam, Map<String, String> params, Class<T> resultClazz)
            throws RequestException {
        // 构建请求体
        Builder requestBuilder = new Builder();
        // 构建请求主体参数
        FormBody body = getFormBody(params).build();
        // 添加请求地址
        requestBuilder.url(url);
        // 请求主体
        requestBuilder.post(body);
        // 请求头部参数
        setHeadParam(requestBuilder, headParam);
        Request request = requestBuilder.build();
        return pckagingResult(request, resultClazz);
    }

    /**
     * POST请求 表单方式提交
     *
     * @param url       请求路径
     * @param headParam 头部参数
     * @param body      自定义请求主体
     * @return
     * @throws RequestException
     */
    public static <T> T sendPOST(String url, Map<String, String> headParam, RequestBody body, Class<T> resultClazz)
            throws RequestException {
        // 构建请求体
        Builder requestBuilder = new Builder();
        // 添加请求地址
        requestBuilder.url(url);
        // 请求主体
        requestBuilder.post(body);
        // 请求头部参数
        setHeadParam(requestBuilder, headParam);
        Request request = requestBuilder.build();
        return pckagingResult(request, resultClazz);
    }


    /**
     * POST请求  Payload{Json}方式提交,适合复杂数据
     *
     * @param url         请求路径
     * @param headParam   头部参数
     * @param params      请求参数
     * @param resultClazz 返回封装对象
     * @param <T>
     * @return
     * @throws RequestException
     */
    public static <T> T sendPOSTJson(String url, Map<String, String> headParam, Map<String, ?> params, Class<T> resultClazz)
            throws RequestException {
        return sendPOSTJson(url, headParam, JSON.toJSONString(params), resultClazz);
    }

    public static <T> T sendPOSTJson(String url, Map<String, String> headParam, String params, Class<T> resultClazz)
            throws RequestException {
        // 构建请求体
        Builder requestBuilder = new Builder();
        // 构建请求主体参数
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), params);
        // 添加请求地址
        requestBuilder.url(url);
        // 请求主体
        requestBuilder.post(body);
        // 请求头部参数
        setHeadParam(requestBuilder, headParam);
        Request request = requestBuilder.build();
        return pckagingResult(request, resultClazz);
    }

    /**
     * 设置get参数
     *
     * @param url
     * @param param
     * @return
     */
    private static String setGetParam(String url, Map<String, String> param) {
        if (param != null && param.size() > 0) {
            StringBuilder builder = new StringBuilder();
            for (String key : param.keySet()) {
                builder.append(key).append("=").append(param.get(key)).append("&");
            }
            url = url + "?" + builder.toString().substring(0, builder.length() - 1);
        }
        return url;
    }

    /**
     * get请求
     *
     * @param url       请求路径
     * @param headParam 头部参数
     * @param param     请求参数
     * @return
     * @throws RequestException
     */
    public static <T> T sendGET(String url, Map<String, String> headParam, Map<String, String> param, Class<T> resultClazz)
            throws RequestException {
        Builder requestBuilder = new Builder();
        // get参数
        url = setGetParam(url, param);
        // 请求头部参数
        setHeadParam(requestBuilder, headParam);
        // 构建请求体
        Request request = requestBuilder.url(url).get().build();
        return pckagingResult(request, resultClazz);
    }

    /**
     * 上传多张图片及参数
     *
     * @param reqUrl    URL地址
     * @param headParam 头部参数
     * @param params    额外参数
     * @param picKey    上传图片的关键字
     * @param files     图片路径
     */
    public static <T> T sendPostMultipart(String reqUrl, Map<String, String> headParam,
                                          Map<String, String> params, String picKey, List<File> files, Class<T> resultClazz) throws RequestException {
        MultipartBody.Builder multipartBodyBuilder = new MultipartBody.Builder();
        multipartBodyBuilder.setType(MultipartBody.FORM);
        setPostParam(params, multipartBodyBuilder);
        // 遍历files中所有图片绝对路径到builder，并约定key如“upload”作为后台接受多张图片的key
        if (files != null) {
            for (File file : files) {
                String sux = getMimeType(file.getName());
                MediaType mediaType = MediaType.parse(MIME.getMimeType(sux));
                multipartBodyBuilder.addFormDataPart(picKey, file.getName(), RequestBody.create(mediaType, file));
            }
        }
        // 构建请求体
        RequestBody requestBody = multipartBodyBuilder.build();
        Builder requestBuilder = new Builder();
        // 添加请求地址
        requestBuilder.url(reqUrl);
        // 请求主体
        requestBuilder.post(requestBody);
        setHeadParam(requestBuilder, headParam);
        return pckagingResult(requestBuilder.build(), resultClazz);
    }

    /**
     * 获取文件类型
     *
     * @param fileName 文件名称
     * @return
     */
    public static String getMimeType(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

}
