package net.lulihu.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.StrKit;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

@Slf4j
public class RequestExpandWrapperUtil {

    /**
     * 请求参数封装
     *
     * @param request 请求
     * @param clazz   返回值类型
     */
    public static <T> List<T> requestParams(HttpServletRequest request, Class<T> clazz) {
        // 从请求体中获取流数据
        if (request instanceof HttpServletRequestExpandWrapper) {
            HttpServletRequestExpandWrapper requestWrapper = (HttpServletRequestExpandWrapper) request;
            String requestStr = requestWrapper.getRequestStr();
            if (StrKit.isNotEmpty(requestStr) && !requestStr.equals("null")) {
                return JSON.parseArray(requestStr, clazz);
            }
        }
        return new ArrayList<>();
    }

    /**
     * 请求参数封装
     *
     * @param request 请求
     * @param result  结果封装
     */
    public static Map<String, Object> requestParams(HttpServletRequest request, Map<String, Object> result) {
        // 从请求体中获取流数据
        if (request instanceof HttpServletRequestExpandWrapper) {
            HttpServletRequestExpandWrapper requestWrapper = (HttpServletRequestExpandWrapper) request;
            String requestStr = requestWrapper.getRequestStr();
            if (StrKit.isNotEmpty(requestStr) && !requestStr.equals("null")) {
                // 循环处理字符串 解决多层数据结构将带有json转义字符封装在String对象中的问题
                JSONObject jsonObject = JSON.parseObject(requestStr);
                jsonObject.entrySet().forEach(entry -> {
                    Object value = entry.getValue();
                    if (value instanceof String)
                        entry.setValue(StrKit.unescapeJson((String) value));
                });
                result.putAll(jsonObject);
            }
        }

        if (!(request instanceof WafRequestWrapper)) request = new WafRequestWrapper(request);
        Enumeration<String> enums = request.getParameterNames();
        while (enums.hasMoreElements()) {
            String paramName = enums.nextElement();
            result.put(paramName, request.getParameter(paramName));
        }
        return result;
    }
}
