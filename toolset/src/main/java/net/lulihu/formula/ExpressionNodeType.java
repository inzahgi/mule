package net.lulihu.formula;

/**
 * 表达式各个字符节点的类型枚举类
 */
public enum ExpressionNodeType {

    /**
     * 空 非法
     */
    Unknown,
    /**
     * + 加号运算
     */
    Plus,
    /**
     * - 减号运算
     */
    Subtract,
    /**
     * * 乘号运算
     */
    MultiPly,
    /**
     * / 除号运算
     */
    Divide,
    /**
     * ( 左括号
     */
    LParentheses,
    /**
     * ) 右括号
     */
    RParentheses,
    /**
     * % (求模,取余)
     */
    Mod,
    /**
     * ^ (次幂)
     */
    Power,
    /**
     * & (按位与)
     */
    BitwiseAnd,
    /**
     * | (按位或)
     */
    BitwiseOr,
    /**
     * && (逻辑与)
     */
    And,
    /**
     * || (逻辑或)
     */
    Or,
    /**
     * ! (逻辑非)
     */
    Not,
    /**
     * == (相等)
     */
    Equal,
    /**
     * != 或 <> (不等于)
     */
    Unequal,
    /**
     * > (大于)
     */
    GT,
    /**
     * < (小于)
     */
    LT,
    /**
     * >= (大于等于)
     */
    GTOrEqual,
    /**
     * <= (小于等于)
     */
    LTOrEqual,
    /**
     * << (左移位)
     */
    LShift,
    /**
     * >> (右移位)
     */
    RShift,
    /**
     * 数值,
     */
    Numeric,
    /**
     * 字符串
     */
    String,
    /**
     * 时间
     */
    Date,
    /**
     * 包含
     */
    Like,
    /**
     * 不包含
     */
    NotLike,
    /**
     * 已什么开始
     */
    StartWith,
    /**
     * 已什么结尾
     */
    EndWith

}
