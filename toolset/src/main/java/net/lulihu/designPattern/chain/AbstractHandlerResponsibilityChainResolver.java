package net.lulihu.designPattern.chain;

import lombok.Getter;

/**
 * 责任链处理解析器
 *
 * @param <T>
 */
public abstract class AbstractHandlerResponsibilityChainResolver<T> implements HandlerResponsibilityChainResolver<T> {

    @Getter
    private String name;

    public AbstractHandlerResponsibilityChainResolver(String name) {
        this.name = name;
    }

    /**
     * 对给定参数进行程序处理
     *
     * @param data 参数
     * @return 返回结果交给下个处理者执行，如果返回为空则链条断开，程序执行结束。
     * <p>
     * <b>返回为空的情况</b>
     * <li>1.处理程序发生异常</li>
     * <li>12.手动指定了程序断开</li>
     */
    public T handlerEvent(T data) throws Exception {
        boolean support = support(data);
        if (support) { // 如果当前对象不能不能处理则交个下一个对象处理
            boolean proceed;
            try {
                resolve(data);  // 执行处理回调
                data = after(data);  // 处理结束回调
                proceed = proceed(data);  // 判断是否继续向下处理
            } catch (ChainEventException e) {
                proceed = error(data, e);  // 处理时发生异常
            }
            data = result(data);  //  执行返回回调
            //如果当前对象指定处理程序返回false或者发生异常，抛出链条断开异常。
            if (!proceed) throw new ChainDisconnectedException();
        }
        return data;
    }
}
