package net.lulihu.designPattern.chain;

import net.lulihu.ObjectKit.StrKit;

/**
 * 链条断开异常
 */
public class ChainDisconnectedException extends Exception {

    public ChainDisconnectedException() {
        super();
    }

    public ChainDisconnectedException(String message) {
        super(message);
    }

    public ChainDisconnectedException(String message, Object... objs) {
        this(StrKit.format(message, objs));
    }


    public ChainDisconnectedException(Throwable tx, String message) {
        super(message, tx);
    }

    public ChainDisconnectedException(Throwable tx, String message, Object... objs) {
        this(StrKit.format(message, objs), tx);
    }

}
