package net.lulihu.office.excel;

import lombok.Getter;
import lombok.Setter;
import net.lulihu.ObjectKit.ReflectKit;
import net.lulihu.ObjectKit.CollectionKit;
import net.lulihu.ObjectKit.MapKit;
import net.lulihu.exception.ToolBoxException;
import net.lulihu.Assert;

import java.lang.reflect.Field;
import java.util.*;

public abstract class Excel {

    @Getter
    private List<List<String[]>> data;
    @Getter
    private String[] sheetName;
    /**
     * 是否设置列标题 如果为false sheetHeaderAlias() 方法将失效
     */
    @Setter
    private boolean hasAlias = true;

    public <D> Excel(List<D> data) {
        this("sheet1", data);
    }

    /**
     * 导出excel 对象
     *
     * @param sheetName 单sheet名称
     * @param data      单sheet导出数据
     * @param <D>       支持泛型对象
     */
    public <D> Excel(String sheetName, List<D> data) {
        this(sheetName, null, data);
    }

    /**
     * 导出excel 对象
     *
     * @param sheetName   单sheet名称
     * @param headerAlias 单sheet别名
     * @param data        单sheet导出数据
     * @param <D>         支持泛型对象
     */
    public <D> Excel(String sheetName, LinkedHashMap<String, String> headerAlias, List<D> data) {
        this(new String[]{sheetName}, MapKit.isNotEmpty(headerAlias) ? Collections.singletonList(headerAlias) : null,
                Collections.singletonList(data));
    }

    /**
     * 导出excel 对象
     *
     * @param sheetName   多sheet名称
     * @param headerAlias 多sheet别名
     * @param data        多sheet数据
     * @param <D>         支持泛型对象
     */
    public <D> Excel(String[] sheetName, List<LinkedHashMap<String, String>> headerAlias, List<List<D>> data) {
        Assert.notNull(data, "写出excel数据为空");

        List<LinkedHashMap<String, String>> sheetHeaderAlias = (List<LinkedHashMap<String, String>>)
                CollectionKit.isEmpty(headerAlias, // 如果用户传值为null
                        () -> CollectionKit.isEmpty(sheetHeaderAlias(), // 判断是否使用方法进行设置
                                () -> defaultHeaderAlias(data))); // 填充bean属性名作为格式化信息

        this.data = dataFormat(data, sheetHeaderAlias);
        this.sheetName = sheetName;
    }

    /**
     * 数据格式化
     *
     * @param data        导出数据
     * @param headerAlias 导出
     * @param <D>别名       泛型对象
     * @return 格式化数据结构
     */
    private <D> List<List<String[]>> dataFormat(List<List<D>> data, List<LinkedHashMap<String, String>> headerAlias) {
        Assert.isTrue(data.size() != headerAlias.size(), "数据sheet长度与别名sheet长度不一致");

        // 数据格式化
        List<List<String[]>> result = new ArrayList<>();
        try {
            int xl = data.size();
            for (int x = 0; x < xl; x++) {
                LinkedHashMap<String, String> alias = headerAlias.get(x);
                List<String> fieldNames = new ArrayList<>(alias.keySet());
                int hl = fieldNames.size();

                List<String[]> sheet = new ArrayList<>();
                if (hasAlias)  // 添加标题列
                    sheet.add(alias.entrySet().stream().map(Map.Entry::getValue).toArray(String[]::new));

                // 添加记录
                for (D y : data.get(x)) {
                    String[] d = new String[hl];
                    for (int i = 0; i < hl; i++) {
                        Object fieldValue = ReflectKit.getFieldValue(y, fieldNames.get(i));
                        d[i] = Objects.toString(fieldValue);
                    }
                    sheet.add(d);
                }
                result.add(sheet);
            }
        } catch (IllegalAccessException e) {
            throw new ToolBoxException(e, "解析导出数据错误");
        }
        return result;
    }

    /**
     * 多sheet 首行合并显示内容 如果为null或者空字符串则不显示
     */
    public String[] labelFirstMerge() {
        return null;
    }

    /**
     * 多sheet 设置标题别名,用来对应具体数据
     * key bean的属性名称 value 对应的别名
     * <p>
     * 如果不设置别名则 使用 headerAlias（data）方法生成别名
     */
    public List<LinkedHashMap<String, String>> sheetHeaderAlias() {
        return null;
    }

    /**
     * 默认多sheet 设置标题别名,用来对应具体数据
     * key bean的属性名称 value 对应的别名
     * <p>
     * 如果不设置别名则列顺序将随机排序
     */
    private <D> List<LinkedHashMap<String, String>> defaultHeaderAlias(List<List<D>> data) {
        this.hasAlias = false;

        List<LinkedHashMap<String, String>> result = new ArrayList<>();

        for (List<D> dd : data) {
            LinkedHashMap<String, String> headerAlias = new LinkedHashMap<>();
            List<Field> fields = ReflectKit.getAllFieldsList(dd.get(0).getClass());
            for (Field field : fields) {
                String fieldName = field.getName();
                headerAlias.put(fieldName, fieldName);
            }
            result.add(headerAlias);
        }
        return result;
    }

}