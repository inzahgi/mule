package net.lulihu;


import lombok.extern.slf4j.Slf4j;
import net.lulihu.functional.Consumer;
import net.lulihu.functional.Consumption;

import java.util.Objects;

/**
 * 断言并执行指定操作
 */
@Slf4j
public class AssertKit {

    /**
     * 类似if else表达式
     *
     * @param boo   判断
     * @param True  如果 boo 为true 则执行此表达式
     * @param False 如果 boo 为false 则执行此表达式
     */
    public static void ifElse(boolean boo, Consumption True, Consumption False) {
        if (boo && True != null)
            True.accept();
        else if (!boo && False != null)
            False.accept();
        else
            throw new NullPointerException();
    }

    /**
     * 如果为true则执行指定表达式
     *
     * @param boo   判断
     * @param t     具体的值
     * @param after 表达式 如果表达式为null，则抛出空指针异常
     */

    public static <T> void isTrue(boolean boo, T t, Consumer<T> after) {
        Objects.requireNonNull(after);
        if (boo)
            after.accept(t);
    }

    /**
     * 如果为true则执行指定表达式
     *
     * @param boo   判断
     * @param after 表达式 如果表达式为null，则抛出空指针异常
     */
    public static void isTrue(boolean boo, Consumption after) {
        Objects.requireNonNull(after);
        if (boo)
            after.accept();
    }

}
