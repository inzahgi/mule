package net.lulihu.eureka_service.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;


/**
 * 问题描述：
 * Eureka Client 无法在Server 端注册，报failure with status code 403; retrying on another server if available
 * <p>
 * 解决方法：
 * Eureka 默认启用了csrf检验，可以将其disable调，
 * 需要要在eureka Server端新建一个WebSecurityConfigurerAdapter的继承，并加上@EnableWebSecurity注解
 * <p>
 * CSRF（Cross-site request forgery）跨站请求伪造，也被称为“One Click Attack”或者Session Riding，
 * 通常缩写为CSRF或者XSRF，是一种对网站的恶意利用。
 */
@EnableWebSecurity
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        super.configure(http);
    }

}