package net.lulihu.mule.tccSpringcloud.demo.order.service.impl;

import net.lulihu.ObjectKit.IDGeneratorKit;
import net.lulihu.common_util.exception.BusinessException;
import net.lulihu.mule.tccSpringcloud.demo.order.client.AccountClient;
import net.lulihu.mule.tccSpringcloud.demo.order.client.InventoryClient;
import net.lulihu.mule.tccSpringcloud.demo.order.dao.OrderMapper;
import net.lulihu.mule.tccSpringcloud.demo.order.dao.OrderProductMapper;
import net.lulihu.mule.tccSpringcloud.demo.order.model.bo.Inventory;
import net.lulihu.mule.tccSpringcloud.demo.order.model.pojo.Order;
import net.lulihu.mule.tccSpringcloud.demo.order.model.pojo.OrderProduct;
import net.lulihu.mule.tccSpringcloud.demo.order.model.vo.CreateOrder;
import net.lulihu.mule.tccSpringcloud.demo.order.service.PressureTestService;
import net.lulihu.mule.tccTransaction.annotation.MuleTcc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PressureTestServiceImpl implements PressureTestService {

    @Autowired
    private AccountClient accountClient;
    @Autowired
    private InventoryClient inventoryClient;

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderProductMapper orderProductMapper;

    @MuleTcc
    @Override
    public String createOrder(CreateOrder createOrder) throws BusinessException {
        // 1.从库存系统获取商品价格清单，并且减少库存，计算价格
        List<Inventory> inventories = inventoryClient.getProductInventory(createOrder.getProducts()).getDataList(Inventory.class);

        String orderNumber = IDGeneratorKit.getStr();
        double totalPrice = 0D;

        List<OrderProduct> orderProducts = new ArrayList<>(inventories.size());
        for (Inventory inventory : inventories) {
            OrderProduct orderProduct = new OrderProduct();
            orderProduct.setOrderNumber(orderNumber);
            orderProduct.setProductNumber(inventory.getProductNumber());
            orderProduct.setQuantity(inventory.getQuantity());
            orderProduct.setPrice(inventory.getPrice().doubleValue());
            orderProducts.add(orderProduct);
            // 计算总价
            totalPrice += inventory.getQuantity() * inventory.getPrice().doubleValue();
        }
        Order order = new Order();
        order.setOrderNumber(orderNumber);
        order.setUserId(createOrder.getUserId());
        order.setTotalPrice(totalPrice);
        // 2. 保存订单 返回订单编号
        orderMapper.save(order);
        orderProductMapper.batchSave(orderProducts);
        return orderNumber;
    }

}
