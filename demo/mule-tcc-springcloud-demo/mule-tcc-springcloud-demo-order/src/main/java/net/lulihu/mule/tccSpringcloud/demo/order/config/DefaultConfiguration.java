package net.lulihu.mule.tccSpringcloud.demo.order.config;

import feign.Logger;
import lombok.extern.slf4j.Slf4j;
import net.lulihu.common_util.controller_result.DefaultRestfulResponseBodyAdvice;
import net.lulihu.common_util.converter.DefaultFastJsonConfig;
import net.lulihu.common_util.fastJson.FastJsonHttpMessageResolver;
import net.lulihu.common_util.spring.SpringContextHolder;
import net.lulihu.http.HttpServletRequestExpandFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * 定义一些默认配置
 */
@Slf4j
@Import({DefaultFastJsonConfig.class, FastJsonHttpMessageResolver.class})
@Configuration
public class DefaultConfiguration {

    public DefaultConfiguration() {
        log.info("==== 开始加载默认配置 ====");
    }

    /**
     * feign日志级别
     * <ul>
     * <li> NONE，无记录（DEFAULT）。 </li>
     * <li> BASIC，只记录请求方法和URL以及响应状态代码和执行时间。 </li>
     * <li> HEADERS，记录基本信息以及请求和响应标头。 </li>
     * <li> FULL，记录请求和响应的头文件，正文和元数据。 </li>
     * </ul>
     * <p>
     * 注意:Feign日志记录仅响应DEBUG级别。
     */
    @Bean
    public Logger.Level feignLoggerLevel() {
        Logger.Level level = Logger.Level.FULL;
        log.info("==== 设置feign日志级别:{} ====", level);
        return level;
    }

    /**
     * HttpServletRequest拓展对象过滤器
     */
    @Bean
    public HttpServletRequestExpandFilter requestBodyReaderFilter() {
        log.info("==== 加载HttpServletRequest拓展对象过滤器 ====");
        return new HttpServletRequestExpandFilter();
    }

    /**
     * Spring的ApplicationContext的持有者
     */
    @Bean
    public SpringContextHolder springContextHolder() {
        log.info("==== 加载ApplicationContext的持有者 ====");
        return new SpringContextHolder();
    }

    /**
     * 接口返回值处理
     */
    @Bean
    public DefaultRestfulResponseBodyAdvice defaultRestfulResponseBodyAdvice() {
        log.info("==== 加载接口返回值处理器 ====");
        return new DefaultRestfulResponseBodyAdvice();
    }

}