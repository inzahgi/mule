package net.lulihu.mule.tccSpringcloud.demo.order.dao;

import net.lulihu.mule.tccSpringcloud.demo.order.model.pojo.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderMapper {

    int save(Order order);

}
