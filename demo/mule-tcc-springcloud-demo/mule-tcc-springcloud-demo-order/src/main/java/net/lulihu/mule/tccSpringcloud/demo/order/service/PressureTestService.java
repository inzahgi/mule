package net.lulihu.mule.tccSpringcloud.demo.order.service;

import net.lulihu.common_util.exception.BusinessException;
import net.lulihu.mule.tccSpringcloud.demo.order.model.vo.CreateOrder;

public interface PressureTestService {


    /**
     * 创建订单
     *
     * @param order 产品
     * @return 返回订单编号
     */
    String createOrder(CreateOrder order) throws BusinessException;
}
