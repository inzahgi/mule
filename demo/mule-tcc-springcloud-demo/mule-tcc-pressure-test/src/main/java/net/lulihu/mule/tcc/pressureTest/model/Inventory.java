package net.lulihu.mule.tcc.pressureTest.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 库存表
 */
@Data
@NoArgsConstructor
@ToString
public class Inventory {

    /**
     * 商品编号
     */
    private String productNumber;

    /**
     * 商品数量
     */
    private Integer quantity;

    /**
     * 单价
     */
    private Number price;

}
