package net.lulihu.mule.tcc.pressureTest.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * 购买产品
 */
@Data
@ToString
@NoArgsConstructor
public class CreateOrder {

    /**
     * 商品集
     */
    private List<Product> products;

    /**
     * 用户id
     */
    private Integer userId;

    @Data
    @NoArgsConstructor
    public static class Product {

        /**
         * 商品编号
         */
        private String productNumber;

        /**
         * 商品数量
         */
        private Integer quantity;

    }
}
