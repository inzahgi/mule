package net.lulihu.common_util.validated;


import lombok.extern.slf4j.Slf4j;
import net.lulihu.common_util.validated.annotation.DateStrFormat;
import net.lulihu.common_util.validated.annotation.NotNull;
import net.lulihu.common_util.validated.annotation.NumberRange;
import net.lulihu.common_util.validated.annotation.StringRange;
import net.lulihu.exception.ExceptionEnum;

import java.lang.annotation.Annotation;

import static net.lulihu.common_util.validated.ValidatedUtil.*;

/**
 * 注解检查工具
 */
@Slf4j
public class AnnotationValidated {

    /**
     * 执行注解检查
     *
     * @param ann              注解
     * @param value            属性值
     * @param defaultException 不匹配时抛出指定参数异常
     */
    public static void examine(Annotation ann, Object value, ExceptionEnum defaultException) {
        if (ann instanceof NotNull) //不能为空
            ParamValidated.isNotEmpty(defaultException, ((NotNull) ann).message(), value);
        if (ann instanceof DateStrFormat) //字符串类型是否匹配
            dateStrFormat((String) value, (DateStrFormat) ann, defaultException);
        if (ann instanceof StringRange) // 字符串取值范围
            stringRange(value, (StringRange) ann, defaultException);
        if (ann instanceof NumberRange) //数值类型取值范围
            numberRange(value, (NumberRange) ann, defaultException);
    }

}
