package net.lulihu.common_util.fastJson;

import java.lang.annotation.*;

/**
 * 应用在方法参数上，使用fastJson解析请求参数
 * <p>
 * 注意应用的方法参数只能为javaBean对象
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FastJson {
}
