package net.lulihu.common_util.validated.spring;

import net.lulihu.common_util.validated.annotation.DateStrFormat;

import javax.validation.ConstraintValidator;

/**
 * 时间格式校验
 */
public class DateStrFormatValidator extends Validator<String> implements ConstraintValidator<DateStrFormat, String> {

    public void initialize(DateStrFormat annotation) {
        setAnnotation(annotation);
    }
}
