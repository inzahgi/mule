package net.lulihu.common_util;


import net.lulihu.common_util.spring.SpringHttpKit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 控制层基础抽象类
 */
public abstract class AbstractController {

    public HttpServletRequest getHttpServletRequest() {
        return SpringHttpKit.getRequest();
    }

    public HttpServletResponse getHttpServletResponse() {
        return SpringHttpKit.getResponse();
    }

}
