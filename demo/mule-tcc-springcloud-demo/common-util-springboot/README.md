# common-util-springboot

#### 介绍
针对spring boot封装的通用方法工具

#### 使用说明
    
    Result(通用返回值封装)
    DefaultRestfulResponseBodyAdvice(返回值处理器)
    DefaultFastJsonConfig(FastJson请求结果序列化配置)
    StringToDateConverter(请求时间转Date对象转换器)
    BusinessExceptionEnum(通用异常枚举封装)
    BusinessExceptionHandler(常用业务异常拦截器)
    FastJsonHttpMessageResolver(FastJson请求解析器)
    SpringContextHolder(Spirng上下文持有者)
    SpringHttpKit(SpringHttp工具集)
    
    支持如下参数检查注解:
     @DateStrFormat
     @NotNull
     @NumberRange
     @StringRange
 
