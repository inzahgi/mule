package net.lulihu.mule.tccSpringcloud.demo.inventory.service.impl;

import net.lulihu.ObjectKit.StrKit;
import net.lulihu.common_util.exception.BusinessException;
import net.lulihu.mule.tccSpringcloud.demo.inventory.dao.InventoryMapper;
import net.lulihu.mule.tccSpringcloud.demo.inventory.model.pojo.Inventory;
import net.lulihu.mule.tccSpringcloud.demo.inventory.model.vo.Product;
import net.lulihu.mule.tccSpringcloud.demo.inventory.service.PressureTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PressureTestServiceImpl implements PressureTestService {

    @Autowired
    private InventoryMapper inventoryMapper;

    @Override
    public List<Inventory> getProductInventory(List<Product> products) {
        String productNumbers = StrKit.listStitchingStr(products, product -> "'" + product.getProductNumber() + "'");
        List<Inventory> inventories = inventoryMapper.batchGetProductInventory(productNumbers);
        // 数量相等 减少库存
        int size = inventories.size();
        if (size == products.size()) {
            for (int i = size - 1; i >= 0; i--) {
                Product product = products.get(i);
                for (Inventory inventory : inventories) {
                    if (product.getProductNumber().equals(inventory.getProductNumber())) {
                        int row = inventoryMapper.decreaseStock(product);
                        if (row <= 0) throw new BusinessException("商品库存不足...");
                        inventory.setQuantity(product.getQuantity());
                        products.remove(i);
                    }
                }
            }
            // 某商品已经下架
        } else throw new BusinessException("部分商品已被下架，请重新选择...");
        return inventories;
    }

    @Override
    public List<Inventory> getProductList() {
        return inventoryMapper.getProductList();
    }

}
