package net.lulihu.mule.tccSpringcloud.demo.inventory.model.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 库存表
 */
@Data
@NoArgsConstructor
public class Inventory {

    /**
     * 商品编号
     */
    private String productNumber;

    /**
     * 商品数量
     */
    private Integer quantity;

    /**
     * 单价
     */
    private Double price;

}
