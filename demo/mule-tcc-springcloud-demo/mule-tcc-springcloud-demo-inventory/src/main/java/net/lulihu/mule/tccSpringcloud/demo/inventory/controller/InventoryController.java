package net.lulihu.mule.tccSpringcloud.demo.inventory.controller;

import net.lulihu.common_util.AbstractController;
import net.lulihu.mule.tccSpringcloud.demo.inventory.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InventoryController extends AbstractController {

    @Autowired
    private InventoryService inventoryService;

    @RequestMapping("getInventory")
    public Object getInventory() {
        return inventoryService.getInventory();
    }

}
