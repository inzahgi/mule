package net.lulihu.mule.tccSpringcloud.demo.account.controller;

import net.lulihu.mule.tccTransaction.annotation.MuleTcc;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class AccountController {


    private AtomicInteger atomicInteger = new AtomicInteger(0);

    @RequestMapping("getBalance")
    @MuleTcc(confirmMethod = "getBalanceConfirmMethod", cancelMethod = "getBalanceCancelMethod")
    public Integer getBalance() {
        Integer.valueOf("a");

        return 500;
    }


    public void getBalanceConfirmMethod() {
        System.out.println("执行成功");
    }


    public void getBalanceCancelMethod() {
        System.out.println("取消回滚");
    }

}
