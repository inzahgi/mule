package net.lulihu.demo.mule_tcc_helloWord;

import net.lulihu.ObjectKit.ObjectKit;
import net.lulihu.mule.tccTransaction.MuleTccConfig;
import net.lulihu.mule.tccTransaction.MuleTccTransactionBootApplication;
import net.lulihu.mule.tccTransaction.annotation.MuleTcc;
import net.lulihu.mule.tccTransaction.exception.NotFindSuitableObjectException;
import net.lulihu.mule.tccTransaction.kit.ObjectContextHolder;
import net.lulihu.mule.tccTransaction.service.MuleTccBootService;
import net.lulihu.mule.tccTransaction.service.TransactionCoordinatorService;
import net.lulihu.mule.tccTransaction.service.TransactionHandlerService;
import net.lulihu.mule.tccTransaction.service.TransactionMethodProxyService;
import net.lulihu.mule.tccTransaction.service.coordinator.DefaultTransactionCoordinatorServiceImpl;
import net.lulihu.mule.tccTransaction.service.impl.DefaultMuleTccBootServiceImpl;
import net.lulihu.mule.tccTransaction.service.impl.DefaultTransactionMethodProxyServiceImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * main 启动 Mule Tcc
 */
public class HelloWord {

    private static final Map<String, Object> objectContext = new ConcurrentHashMap<>();

    public static void main(String[] args) {
        try {
            start();// 启动

            // 事务代理方法
            DefaultTransactionMethodProxyServiceImpl transactionMethodProxyService = new DefaultTransactionMethodProxyServiceImpl();

            ExecutorProxyFactory rubberProxy = new ExecutorProxyFactory(new Rubber(), transactionMethodProxyService);
            Car carRubber = (Car) rubberProxy.proxy();

            ExecutorProxyFactory tireProxy = new ExecutorProxyFactory(new Tire(carRubber), transactionMethodProxyService);
            Car carTire = (Car) tireProxy.proxy();

            ExecutorProxyFactory carInter = new ExecutorProxyFactory(new Train(carTire), transactionMethodProxyService);
            Car car = (Car) carInter.proxy();
            int weight = car.weight();
            System.out.println(weight);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.exit(-1);
        }
    }

    public static void start() {
        MuleTccConfig config = new MuleTccConfig();
        MuleTccConfig.DbConfig dbConfig = new MuleTccConfig.DbConfig();
        dbConfig.setUrl("jdbc:mysql://127.0.0.1:3306/tcc?useUnicode=true&amp;characterEncoding=utf8");
        dbConfig.setUsername("root");
        dbConfig.setPassword("123456");

        config.setDbConfig(dbConfig);
        config.setApplicationName("test");

        ObjectContextHolder.getInstance().setObjectContext(new ObjectContextHolder.ObjectContext() {

            @Override
            public <T> T getBean(Class<T> requiredType) throws NotFindSuitableObjectException {
                if (!objectContext.containsKey(requiredType.getName()))
                    throw new NotFindSuitableObjectException();
                return (T) objectContext.get(requiredType.getName());
            }
        });


        TransactionCoordinatorService coordinatorService = new DefaultTransactionCoordinatorServiceImpl();
        MuleTccBootService muleTccBootService = new DefaultMuleTccBootServiceImpl(coordinatorService);
        MuleTccTransactionBootApplication application = new MuleTccTransactionBootApplication(muleTccBootService, config);
        application.start();
    }

    public static class ExecutorProxyFactory {

        // 实际执行者对象
        private Object obj;
        // 事务方法代理
        private final TransactionMethodProxyService transactionMethodProxyService;

        public ExecutorProxyFactory(Object obj, TransactionMethodProxyService transactionMethodProxyService) {
            this.obj = obj;
            this.transactionMethodProxyService = transactionMethodProxyService;
        }

        public Object proxy() {
            // 类加载器有哪一个类加载器负责加载
            ClassLoader loader = obj.getClass().getClassLoader();
            // 要代理的对象类型
            Class[] interfaces = obj.getClass().getInterfaces();
            // 当调用代理对象方法时，执行调用的方法。
            InvocationHandler invocation = (proxy, method, args) -> {
                MuleTcc annotation = method.getAnnotation(MuleTcc.class);
                if (annotation == null) return method.invoke(obj, args);

                TransactionMethodProxyService methodProxyService = transactionMethodProxyService;
                TransactionHandlerService handlerService = methodProxyService.beforeTransactionHandler();
                boolean proceed = methodProxyService.before(handlerService, obj.getClass(), method, args);
                try {
                    if (proceed) {
                        Object result = method.invoke(obj, args);
                        return methodProxyService.result(handlerService, result);
                    }
                } catch (Exception e) {
                    methodProxyService.exception(handlerService, e);
                } finally {
                    methodProxyService.after(handlerService);
                }
                return ObjectKit.getDefaultValue(method.getReturnType());
            };
            return Proxy.newProxyInstance(loader, interfaces, invocation);
        }
    }


    public interface Car {

        @MuleTcc(confirmMethod = "confirm", cancelMethod = "cancel")
        int weight();

        void confirm();

        void cancel();

    }


    public static class Train implements Car {

        private Car cartire;

        public Train(Car cartire) {
            this.cartire = cartire;
            HelloWord.objectContext.put(getClass().getName(), this);
        }

        @Override
        public int weight() {
            return 1000 + cartire.weight();
        }

        @Override
        public void confirm() {
            System.out.println("获取火车重量方法执行成功");
        }

        @Override
        public void cancel() {
            System.out.println("获取火车重量方法执行失败");
        }

    }

    public static class Tire implements Car {

        private final Car rubber;

        public Tire(Car rubber) {
            this.rubber = rubber;
            HelloWord.objectContext.put(getClass().getName(), this);
        }

        @Override
        public int weight() {
            return 10 + rubber.weight();
        }

        @Override
        public void confirm() {
            System.out.println("获取车轮重量方法执行成功");
        }

        @Override
        public void cancel() {
            System.out.println("获取车轮重量方法执行失败");
        }

    }

    public static class Rubber implements Car {
        public Rubber() {
            HelloWord.objectContext.put(getClass().getName(), this);
        }

        @Override
        public int weight() {
            return 5;
        }

        @Override
        public void confirm() {
            System.out.println("获取橡胶重量方法执行成功");
        }

        @Override
        public void cancel() {
            System.out.println("获取橡胶重量方法执行失败");
        }
    }


}
