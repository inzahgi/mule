package net.lulihu.mule.tccTransaction.kit;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.Assert;
import net.lulihu.functional.Consumption;
import net.lulihu.mule.tccTransaction.enums.MuleActionEnum;
import net.lulihu.mule.tccTransaction.exception.ParticipantMethodExeFailureException;
import net.lulihu.mule.tccTransaction.model.MuleParticipant;
import net.lulihu.mule.tccTransaction.model.MuleTransaction;
import net.lulihu.mule.tccTransaction.model.TransactionContext;
import net.lulihu.mule.tccTransaction.service.TransactionCoordinatorService;
import net.lulihu.ObjectKit.CollectionKit;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.ObjectKit.MethodKit;
import net.lulihu.ObjectKit.StrKit;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * 执行参与者方法工具集
 */
@Slf4j
public class ParticipantKit {


    /**
     * 执行参与者方法
     *
     * @param transactionCoordinatorService 事务协调器
     * @param status                        事务状态 true执行确认方法，false执行取消方法
     * @param retryMax                      最大重试次数
     * @param muleTransaction               事务记录
     */
    public static void exeParticipantMethod(TransactionCoordinatorService transactionCoordinatorService,
                                            boolean status, Integer retryMax, MuleTransaction muleTransaction) {
        // 修改事物状态
        transactionCoordinatorService.updateStatus(muleTransaction, null);

        List<MuleParticipant> participants = muleTransaction.getParticipants();
        // 执行参与者方法
        exeParticipantMethod(status, retryMax, 1, participants);
        // 如果参与者方法全部执行成功则删除事务记录
        if (CollectionKit.isEmpty(participants))
            transactionCoordinatorService.delete(muleTransaction, null);
        else   // 反之修改参与者信息，保存执行失败的
            transactionCoordinatorService.updateParticipant(muleTransaction, null);
    }

    /**
     * 执行参与者方法
     *
     * @param status           事务状态 true执行确认方法，false执行取消方法
     * @param retryNum         本次出错重复次数
     *                         重试次数从1开始,1既执行一次不重复，2为执行一次最多重试一次，以此类推
     * @param muleParticipants 参与者记录集
     */
    public static void exeParticipantMethod(boolean status, int retryNum, List<MuleParticipant> muleParticipants) throws ParticipantMethodExeFailureException {
        exeParticipantMethod(status, null, retryNum, muleParticipants);
    }

    /**
     * 执行参与者方法
     *
     * @param status           事务状态 true执行确认方法，false执行取消方法
     * @param retryMax         参与者指定方法执行错误最大重复次数
     * @param retryNum         本次出错重复次数
     *                         重试次数从1开始,1既执行一次不重复，2为执行一次最多重试一次，以此类推
     * @param muleParticipants 参与者记录集
     */
    public static void exeParticipantMethod(boolean status, Integer retryMax,
                                            Integer retryNum, List<MuleParticipant> muleParticipants) throws ParticipantMethodExeFailureException {
        Assert.notNull(retryNum, "重试次数不可以为空...");

        if (CollectionKit.isEmpty(muleParticipants)) return;

        for (int i = muleParticipants.size() - 1; i >= 0; i--) {
            MuleParticipant muleParticipant = muleParticipants.get(i);
            Integer retries = muleParticipant.getRetries();

            // 达到最大重复次数，跳过
            if (retryMax != null && retries >= retryMax) continue;
            if (retryMax != null && retries + retryNum > retryMax) {
                int oldRetryNum = retryNum;
                retryNum = retryMax - retries;
                LogKit.warn(log, "当前以及重试次数为{}，本次设定重复次数为{}，" +
                        "以及超过最大重复次数{}，自动降级重试次数为{}", retries, oldRetryNum, retryMax, retryNum);
            }
            // 根据参数修改事务上下文状态
            Boolean compensation = muleParticipant.getParticipateCompensation();
            TransactionContext context = TransactionContextLocalKit.get();
            if (!compensation) context.setAction(MuleActionEnum.DELETE.getCode());     // 不参与则删除
            else context.setAction(status ? MuleActionEnum.CONFIRMING.getCode() : MuleActionEnum.CANCELING.getCode());

            try {  // 执行目标方法
                Object obj = ObjectContextHolder.getInstance().getBean(muleParticipant.getTargetClass());
                String targetMethod = muleParticipant.getTargetMethod();
                // 执行目标方法
                if (StrKit.notBlank(targetMethod)) {
                    exeParticipantMethod(retryNum, obj, targetMethod, muleParticipant.getArgs(), muleParticipant.getParameterTypes());
                } else {// 执行目标制定确认或者取消方法
                    String methodName = (status ? muleParticipant.getConfirmMethod() : muleParticipant.getCancelMethod());
                    if (StrKit.notBlank(methodName))
                        exeParticipantMethodParentalAppointment(retryNum, obj, methodName, muleParticipant.getArgs(), muleParticipant.getParameterTypes());
                }
                // 执行成功删除参与者
                muleParticipants.remove(i);
            } catch (ParticipantMethodExeFailureException e) {
                muleParticipant.setRetries(retries + retryNum);
                LogKit.error(log, e.getMessage(), e.getCause());
            }
        }
    }

    /**
     * 执行参与者指定方法 (使用双亲委派机制，优先执行存在指定参数类型的方法，不存在则执行无参方法，都不存在则打印错误异常)
     *
     * @param retryMax       参与者指定方法执行错误最大重复次数
     *                       重试次数从1开始,1既执行一次不重复，2为执行一次最多重试一次，以此类推
     * @param object         参与者对象
     * @param methodName     参与者方法名称
     * @param args           参与者方法参数
     * @param parameterTypes 参与者方法参数类型
     */
    public static void exeParticipantMethodParentalAppointment(int retryMax, Object object, String methodName, Object[] args, Class<?>[] parameterTypes)
            throws ParticipantMethodExeFailureException {
        // 优先判断有参数的方法
        if (!MethodKit.existSpecifiedMethod(object.getClass(), methodName, parameterTypes)) {
            if (CollectionKit.isEmpty(args))
                throw new ParticipantMethodExeFailureException("参与者【{}】方法【{}()】都不存在或未正确的定义...", object.getClass(), methodName);

            // 无参方法
            args = new Object[0];
            parameterTypes = new Class[0];
            if (!MethodKit.existSpecifiedMethod(object.getClass(), methodName, parameterTypes)) {
                throw new ParticipantMethodExeFailureException("参与者【{}】方法【{}({} ) 和 {}()】都不存在或未正确的定义...", object.getClass(), methodName,
                        StrKit.listStitchingStr(type -> " " + type, parameterTypes), methodName);
            }
        }
        // 执行参与者方法
        exeParticipantMethod(retryMax, object, methodName, args, parameterTypes);
    }

    /**
     * 执行参与者指定方法
     *
     * @param retryMax       参与者指定方法执行错误最大重复次数
     *                       重试次数从1开始,1既执行一次不重复，2为执行一次最多重试一次，以此类推
     * @param object         参与者对象
     * @param methodName     参与者方法名称
     * @param args           参与者方法参数
     * @param parameterTypes 参与者方法参数类型
     */
    public static void exeParticipantMethod(int retryMax, Object object, String methodName,
                                            Object[] args, Class<?>[] parameterTypes) throws ParticipantMethodExeFailureException {
        try {
            MethodKit.invokeExactMethod(object, methodName, args, parameterTypes);
        } catch (NoSuchMethodException e) {
            recursiveExeParticipantMethod(retryMax, object, methodName,
                    args, parameterTypes, e,
                    () -> LogKit.error(log, "参与者【{}】方法【{}({} )】不存在或未正确的定义...",
                            () -> new Object[]{object.getClass(), methodName, StrKit.listStitchingStr(type -> " " + type, parameterTypes), e}));
        } catch (IllegalAccessException e) {
            recursiveExeParticipantMethod(retryMax, object, methodName,
                    args, parameterTypes, e,
                    () -> LogKit.error(log, "参与者【{}】方法【{}】无法通过反射访问...",
                            () -> new Object[]{object.getClass(), methodName, e}));
        } catch (InvocationTargetException e) {
            recursiveExeParticipantMethod(retryMax, object, methodName,
                    args, parameterTypes, e,
                    () -> LogKit.warn(log, "执行参与者【{}】方法【{}】时发生例外:【{} : {}】，剩余重试次数{}",
                            () -> new Object[]{object.getClass(), methodName, e.getCause().getClass().getSimpleName(), e.getCause().getMessage(), retryMax - 1}));
        }
    }

    /**
     * 递归执行参与者方法
     *
     * @param retryMax       参与者指定方法执行错误最大重复次数
     *                       重试次数从1开始,1既执行一次不重复，2为执行一次最多重试一次，以此类推
     * @param object         参与者对象
     * @param methodName     参与者方法名称
     * @param args           参与者方法参数
     * @param parameterTypes 参与者方法参数类型
     * @param e              执行参与者方法异常
     * @param consumption    递归执行表达式方法
     */
    private static void recursiveExeParticipantMethod(int retryMax, Object object, String methodName,
                                                      Object[] args, Class<?>[] parameterTypes,
                                                      Exception e, Consumption consumption) {
        if (retryMax <= 1) {
            throw new ParticipantMethodExeFailureException(e, "执行参与者【{}】方法【{}】多次重试后依然发生例外... ", object.getClass(), methodName);
        }
        consumption.accept();
        exeParticipantMethod(retryMax - 1, object, methodName, args, parameterTypes);
    }

}
