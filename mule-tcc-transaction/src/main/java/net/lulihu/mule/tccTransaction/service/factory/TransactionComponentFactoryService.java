package net.lulihu.mule.tccTransaction.service.factory;

import net.lulihu.mule.tccTransaction.exception.NotFindSuitableObjectException;
import net.lulihu.mule.tccTransaction.model.TransactionContext;
import net.lulihu.mule.tccTransaction.service.TransactionExecutorEventService;
import net.lulihu.mule.tccTransaction.service.TransactionHandlerService;

/**
 * 事务组件工厂服务
 * <p>
 * 负责提供处理事务的组件对象
 */
public interface TransactionComponentFactoryService {

    /**
     * 根据上下文 选举合适的事务处理器
     *
     * @param context 上下文
     * @return 对应的事务处理器
     * @throws NotFindSuitableObjectException 未找到合适的则抛出异常
     */
    TransactionHandlerService electionTransactionHandler(TransactionContext context) throws NotFindSuitableObjectException;

    /**
     * 根据处理程序选举合适的执行者
     *
     * @param handlerService 处理程序
     * @return 执行者
     * @throws NotFindSuitableObjectException 未找到合适的则抛出异常
     */
    TransactionExecutorEventService electionTransactionExecutor(TransactionHandlerService handlerService) throws NotFindSuitableObjectException;

}
