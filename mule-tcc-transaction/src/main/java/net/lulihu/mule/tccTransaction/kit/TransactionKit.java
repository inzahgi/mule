package net.lulihu.mule.tccTransaction.kit;


import net.lulihu.ObjectKit.IDGeneratorKit;
import net.lulihu.ObjectKit.StrKit;
import net.lulihu.ToolUtil;
import net.lulihu.mule.tccTransaction.annotation.MuleTcc;
import net.lulihu.mule.tccTransaction.enums.MuleActionEnum;
import net.lulihu.mule.tccTransaction.enums.MuleRoleEnum;
import net.lulihu.mule.tccTransaction.model.MuleParticipant;
import net.lulihu.mule.tccTransaction.model.MuleTransaction;
import net.lulihu.mule.tccTransaction.model.TransactionContext;

import java.lang.reflect.Method;

/**
 * 事务工具包
 */
public class TransactionKit {

    /**
     * 构建分布式事务参数
     *
     * @param transId   事务id 可为空
     * @param role      事务处理者角色
     * @param beanClass 代理方法的目标对象类型
     * @param method    代理方法
     * @param args      目标方法参数
     * @return 分布式事务参数 对象
     */
    public static MuleTransaction buildMuleTransaction(String transId, MuleRoleEnum role,
                                                       Class<?> beanClass, Method method, Object[] args) {
        MuleTransaction muleTransaction = new MuleTransaction(transId);
        // 事务状态 / 角色
        muleTransaction.setStatus(MuleActionEnum.TRYING.getCode());
        muleTransaction.setRole(role.getCode());
        //  代理对象 / 方法
        Class<?> clazz = method.getDeclaringClass();
        muleTransaction.setTargetClass(clazz.getName());
        muleTransaction.setTargetMethod(method.getName());
        // 添加参与者
        addMuleParticipant(muleTransaction, beanClass, method, args);
        return muleTransaction;
    }

    /**
     * 添加参与者
     *
     * @param muleTransaction 事务记录
     * @param beanClass       代理方法的目标对象类型
     * @param method          被代理的方法
     */
    public static void addMuleParticipant(final MuleTransaction muleTransaction,
                                          final Class<?> beanClass, final Method method,
                                          final Object[] args) {
        MuleTcc muleTcc = method.getAnnotation(MuleTcc.class);

        MuleParticipant muleParticipant = new MuleParticipant();
        muleParticipant.setTransId(muleTransaction.getTransId());
        muleParticipant.setTargetClass(beanClass);

        // 是否执行目标方法
        boolean currentMethod = muleTcc.currentMethod();
        if (currentMethod) {
            muleParticipant.setTargetMethod(method.getName());
        }
        muleParticipant.setArgs(args);
        muleParticipant.setParameterTypes(method.getParameterTypes());
        //  成功回调
        String confirmMethodName = muleTcc.confirmMethod();
        if (StrKit.notBlank(confirmMethodName)) {
            muleParticipant.setConfirmMethod(confirmMethodName);
        }
        // 失败回调
        String cancelMethodName = muleTcc.cancelMethod();
        if (StrKit.notBlank(cancelMethodName)) {
            muleParticipant.setCancelMethod(cancelMethodName);
        }
        // 参与者方法执行发生例外是否参与回滚
        muleParticipant.setParticipateCompensation(muleTcc.exceptionNotRollBack());
        // 添加参与者
        muleTransaction.registerParticipant(muleParticipant);
    }

    /**
     * 构建多服务 事务上下文
     *
     * @param action 事件类型
     * @param role   角色类型
     * @return 事务上下文
     */
    public static TransactionContext buildMuleTransactionContext(MuleActionEnum action, MuleRoleEnum role) {
        String transId = IDGeneratorKit.get().toString() + ToolUtil.getRandomHexString(14);

        TransactionContext context = new TransactionContext();
        context.setTransId(transId);
        context.setAction(action.getCode());
        context.setRole(role.getCode());
        return context;

    }

}
