package net.lulihu.mule.tccTransaction.service;


import net.lulihu.mule.tccTransaction.MuleTccConfig;
import net.lulihu.mule.tccTransaction.enums.RepositorySupportEnum;
import net.lulihu.mule.tccTransaction.model.MuleTransaction;
import net.lulihu.mule.tccTransaction.model.MuleTransactionCompensations;
import net.lulihu.mule.tccTransaction.serializer.ObjectSerializer;

import java.util.List;

/**
 * 事务协调器存储库
 */
public interface TransactionCoordinatorRepositoryService {

    /**
     * 设置序列化程序
     *
     * @param objectSerializer {@linkplain ObjectSerializer}
     */
    void setSerializer(ObjectSerializer objectSerializer);

    /**
     * 存储库枚举类型
     */
    RepositorySupportEnum getScheme();

    /**
     * 初始化协调器储存库
     *
     * @param config 配置
     * @throws Exception 初始化过程中发生任何错误将抛出异常
     */
    void initCoordinatorRepository(MuleTccConfig config) throws Exception;

    /**
     * 保存事务记录
     *
     * @param muleTransaction 事务记录
     * @return true保存成功反之失败
     */
    boolean saveTransactionLog(MuleTransaction muleTransaction);

    /**
     * 修改事务记录状态
     *
     * @param muleTransaction 事务记录
     * @return true保存成功反之失败
     */
    boolean updateMuleTransactionLogStatus(MuleTransaction muleTransaction);

    /**
     * 修改事务参与者
     *
     * @param muleTransaction 事务记录
     * @return true保存成功反之失败
     */
    boolean updateParticipant(MuleTransaction muleTransaction);


    /**
     * 根据事务id获取事务记录
     *
     * @param transId 事务id
     * @return 事务记录
     */
    MuleTransaction getMuleTransactionById(String transId);

    /**
     * 根据事务id删除事务记录
     *
     * @param transId 事务id
     * @return true成功反之失败
     */
    boolean delete(String transId);

    /**
     * 获取所有事务激励
     */
    List<MuleTransaction> getAllMuleTransaction();

    /**
     * 获取事务乐观锁
     *
     * @param transaction         事务信息
     * @param recoverTimeInterval 自动恢复时间间隔
     * @return true为获取成功，反之失败
     */
    boolean getOptimisticLocks(MuleTransaction transaction, Integer recoverTimeInterval);

    /**
     * 保存事务补偿记录
     *
     * @param transactionCompensations 事务补偿信息
     * @return true为成功，反之失败
     */
    boolean saveCompensationsLog(MuleTransactionCompensations transactionCompensations);

    /**
     * 获取事务补偿记录
     *
     * @param transaction 事务信息
     * @return 事务补偿记录
     */
    MuleTransactionCompensations getMuleTransactionCompensationsLog(MuleTransaction transaction);

    /**
     * 删除事务补偿记录
     *
     * @param transactionCompensations 事件补偿记录
     */
    boolean deleteTransactionCompensationsLog(MuleTransactionCompensations transactionCompensations);

    /**
     * 删除多余事务补偿记录
     *
     * @param initialDelay 事务补偿初始延迟时间 单位/秒
     */
    boolean deleteExcessCompensationRecord(int initialDelay);
}
