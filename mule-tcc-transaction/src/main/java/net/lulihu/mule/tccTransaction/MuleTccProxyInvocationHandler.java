package net.lulihu.mule.tccTransaction;

import net.lulihu.ObjectKit.ObjectKit;
import net.lulihu.mule.tccTransaction.annotation.MuleTcc;
import net.lulihu.ObjectKit.KryoKit;
import net.lulihu.mule.tccTransaction.kit.ObjectContextHolder;
import net.lulihu.mule.tccTransaction.service.TransactionHandlerService;
import net.lulihu.mule.tccTransaction.service.TransactionMethodProxyService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Mule Tcc代理执行程序
 */
public class MuleTccProxyInvocationHandler implements InvocationHandler {

    private final TransactionMethodProxyService methodProxyService;

    private final InvocationHandler invocationHandler;

    private final Class<?> beanClass;

    /**
     * 新的实例对象
     *
     * @param methodProxyService 事务代理方法服务
     * @param invocationHandler  对象原本的反向代理处理器
     * @param beanClass          对象类型
     */
    public MuleTccProxyInvocationHandler(TransactionMethodProxyService methodProxyService,
                                         InvocationHandler invocationHandler, Class<?> beanClass) {
        this.methodProxyService = methodProxyService;
        this.invocationHandler = invocationHandler;
        this.beanClass = beanClass;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        MuleTcc muleTcc = method.getAnnotation(MuleTcc.class);

        if (muleTcc == null) return invokeTargetMethod(proxy, method, args);
        // 使用序列化的方式实现深拷贝
        // 因为参数为引用传递如果在方法内部修改该参数值可能导致后续参数不对应
        Object[] cloneArgs = KryoKit.clone(args);

        TransactionHandlerService handlerService = methodProxyService.beforeTransactionHandler();
        boolean proceed = methodProxyService.before(handlerService, beanClass, method, cloneArgs);
        try {
            if (proceed) {
                Object result = invokeTargetMethod(proxy, method, cloneArgs);
                return methodProxyService.result(handlerService, result);
            }
        } catch (Exception e) {
            methodProxyService.exception(handlerService, e);
        } finally {
            methodProxyService.after(handlerService);
        }
        return ObjectKit.getDefaultValue(method.getReturnType());
    }

    /**
     * 执行目标方法
     *
     * @param proxy  代理对象
     * @param method 执行方法
     * @param args   方法参数
     * @return 方法执行结果
     * @throws Throwable 执行过程中出现异常则抛出
     */
    private Object invokeTargetMethod(Object proxy, Method method, Object[] args) throws Throwable {
        if (invocationHandler != null)
            return invocationHandler.invoke(proxy, method, args);
        return method.invoke(ObjectContextHolder.getInstance().getBean(beanClass), args);
    }

    /**
     * 生成代理对象
     *
     * @param methodProxyService 事务代理方法服务
     * @param invocationHandler  对象原本的反向代理处理器
     * @param targetObject       目标对象
     * @return 新的代理对象
     */
    public static Object generateProxyObject(TransactionMethodProxyService methodProxyService,
                                             InvocationHandler invocationHandler,
                                             Object targetObject) {
        return generateProxyObject(methodProxyService, invocationHandler, targetObject, targetObject.getClass());
    }

    /**
     * 生成代理对象
     *
     * @param methodProxyService 事务代理方法服务
     * @param invocationHandler  对象原本的反向代理处理器
     * @param targetObject       目标对象
     * @param beanClass          对象全类型
     * @return 新的代理对象
     */
    public static Object generateProxyObject(TransactionMethodProxyService methodProxyService,
                                             InvocationHandler invocationHandler,
                                             Object targetObject, Class<?> beanClass) {
        Class<?> clazz = targetObject.getClass();
        ClassLoader loader = clazz.getClassLoader(); // 类加载器有哪一个类加载器负责加载
        Class[] interfaces = clazz.getInterfaces();  // 要代理的对象类型
        MuleTccProxyInvocationHandler proxyInvocationHandler =
                new MuleTccProxyInvocationHandler(methodProxyService, invocationHandler, beanClass);
        return Proxy.newProxyInstance(loader, interfaces, proxyInvocationHandler);
    }

}
