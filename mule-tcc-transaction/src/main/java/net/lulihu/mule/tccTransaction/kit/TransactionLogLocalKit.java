package net.lulihu.mule.tccTransaction.kit;

import net.lulihu.ObjectKit.ConcurrentThreadLocalKit;
import net.lulihu.mule.tccTransaction.enums.ThreadLocalEnum;
import net.lulihu.mule.tccTransaction.exception.MuleTccException;
import net.lulihu.mule.tccTransaction.model.MuleTransaction;

/**
 * 本地事务上下文工具集
 */
public class TransactionLogLocalKit {

    private static final ConcurrentThreadLocalKit threadLocal = ConcurrentThreadLocalKit.getInstance();

    private static final String key = ThreadLocalEnum.TRANSACTION_LOG.getDescription();

    /**
     * 构造函数私有化
     */
    private TransactionLogLocalKit() {
        if (threadLocal.containsKey(key))
            throw new MuleTccException("当前线程副本中已经存在指定名称【{}】的数据副本", key);
    }

    /**
     * 设置本地事务记录
     *
     * @param transactionContext 事务上下文
     */
    public static void set(MuleTransaction transactionContext) {
        threadLocal.set(key, transactionContext);
    }

    /**
     * 获取本地事务记录
     */
    public static MuleTransaction get() {
        return (MuleTransaction) threadLocal.get(key);
    }

    /**
     * 删除当前线程的事务记录
     */
    public static void clear() {
        threadLocal.clear(key);
    }

}
