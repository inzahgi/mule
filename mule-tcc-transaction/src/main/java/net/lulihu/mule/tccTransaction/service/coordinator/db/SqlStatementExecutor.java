package net.lulihu.mule.tccTransaction.service.coordinator.db;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.CollectionKit;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.mule.tccTransaction.MuleTccConfig;
import net.lulihu.mule.tccTransaction.enums.RepositorySupportEnum;
import net.lulihu.mule.tccTransaction.exception.MuleTccException;
import net.lulihu.mule.tccTransaction.exception.RepositoryMapperException;
import net.lulihu.mule.tccTransaction.kit.QueryResultSetKit;
import net.lulihu.mule.tccTransaction.kit.RepositoryPathKit;
import net.lulihu.mule.tccTransaction.kit.TableSqlKit;
import net.lulihu.mule.tccTransaction.serializer.ObjectSerializer;

import java.sql.*;
import java.util.*;

/**
 * sql命令语句执行程序
 */
@Slf4j
public class SqlStatementExecutor extends DataSourceManage {

    // 序列化对象
    private ObjectSerializer serializer;

    protected String appName;

    public void initCoordinatorRepository(final MuleTccConfig config) {
        LogKit.debug(log, "事务协调储存库【{}】开始初始化...", () -> new Object[]{componentName()});

        try {
            MuleTccConfig.DbConfig dbConfig = config.getDbConfig();
            // 设置数据源
            setDataSource(dbConfig);
            // 获取服务对应的事务日志db表名称
            String appName = RepositoryPathKit.getAppDbTableName(config.getApplicationName());
            this.appName = appName;

            // 构建对应的数据库建表语句
            String[] tableSqlArray = TableSqlKit.getCreateTableSql(dbConfig.getDriverClassName(), appName);
            LogKit.debug(log, "创建应用程序事务储存表...");

            // 建表
            for (String tableSql : tableSqlArray) {
                executeUpdate(tableSql);
            }
        } catch (Exception e) {
            throw new MuleTccException("初始化协调员储存库发生例外:", e);
        }
    }

    public int executeUpdate(String sql, Object... params) throws SQLException {
        return executeUpdate(sql, CollectionKit.wrap(params));
    }

    /**
     * 执行增删改操作
     *
     * @param sql    语句
     * @param params 参数
     * @return 返回执行影响的条数
     */
    public int executeUpdate(String sql, List<Object> params) throws SQLException {
        Integer row = null;
        try (Connection connection = getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            // 替换命令占位符
            statementReplacement(ps, params);
            // 执行
            row = ps.executeUpdate();
            return row;
        } finally {
            sqlLog(sql, row, params); // 打印日志
        }
    }

    /**
     * 执行查询语句
     *
     * @param resultClass 返回值类型
     * @param sql         语句
     * @param params      参数
     * @return 查询结果封装至返回值中
     */
    private Object executeQuery(Class<?> resultClass, String sql, Object... params) throws Exception {
        return executeQuery(resultClass, sql, CollectionKit.wrap(params));
    }

    private Object executeQuery(Class<?> resultClass, String sql, List<Object> params) throws Exception {
        try (Connection connection = getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            statementReplacement(ps, params);
            try (ResultSet resultSet = ps.executeQuery()) {
                Object result = QueryResultSetKit.putResultSet(serializer, resultSet, resultClass);
                sqlLog(sql, result, params);
                return result;
            } catch (Exception e) {
                sqlLog(sql, null, params);
                throw e;
            }
        }
    }

    /**
     * 执行查询语句
     *
     * @param resultClass 返回值类型
     * @param sql         语句
     * @param params      参数
     * @return 查询结果封装至返回值中
     */
    @SuppressWarnings("unchecked")
    public <T> T executeQueryOne(Class<T> resultClass, String sql, Object... params) throws Exception {
        Object result = executeQuery(resultClass, sql, params);
        if (result instanceof Collection) {
            Collection collection = (Collection) result;
            int size = collection.size();
            if (size == 0) return null;
            if (size == 1) return (T) collection.iterator().next();
            throw new RepositoryMapperException("查询结果为多个无法封装至单个对象【{}】中...", resultClass);
        }
        return (T) result;
    }

    /**
     * 执行查询语句返回多个结果
     *
     * @param resultClass 返回值类型
     * @param sql         语句
     * @param params      参数
     * @return 查询结果封装至返回值中
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> executeQueryList(Class<T> resultClass, String sql, Object... params) throws Exception {
        Object result = executeQuery(resultClass, sql, params);

        if (result instanceof Collection) return (List<T>) result;
        if (result == null) return new ArrayList<>();
        return new ArrayList<>(Collections.singletonList((T) result));
    }

    /**
     * 打印日志
     *
     * @param sql    sql 声明
     * @param result 查询结果
     * @param params 参数
     */
    private void sqlLog(String sql, Object result, List<Object> params) {
        int row = 0;
        if (result != null) row = 1;
        if (result instanceof Collection) row = ((Collection) result).size();
        sqlLog(sql, row, params);
    }

    /**
     * 打印日志
     *
     * @param sql    sql 声明
     * @param row    影响行数/查询行数
     * @param params 参数
     */
    private void sqlLog(String sql, Integer row, List<Object> params) {
        if (!log.isDebugEnabled()) return;

        // 参数拼接
        StringBuilder builder = new StringBuilder();
        for (Object param : params) {
            boolean boo = (param == null);
            Object value = boo ? "" : param;
            String simpleName = boo ? "NULL" : param.getClass().getSimpleName();
            builder.append(", ").append(value).append("(").append(simpleName).append(")");
        }
        builder.delete(0, 2);

        LogKit.debug(log, "Statement ==> : {}", () -> new Object[]{beautifySQL(sql)});
        LogKit.debug(log, "Params    ==> : {}", () -> new Object[]{builder.toString()});
        LogKit.debug(log, "Rows      ==> : {}", () -> new Object[]{row == null ? "" : row});
    }

    /**
     * sql 美化
     */
    private String beautifySQL(String sql) {
        return sql.replaceAll("[\\s\n ]+", " ");
    }

    /**
     * sql 命令占位符替换
     *
     * @param ps     sql声明
     * @param params 参数
     * @throws SQLException 如果参数索引与SQL语句中的参数标记不对应;
     *                      如果发生数据库访问错误;在封闭的PreparedStatement上调用此方法，
     *                      或者给定对象的类型不明确 抛出异常
     */
    private void statementReplacement(PreparedStatement ps, List<Object> params) throws SQLException {
        if (params == null) return;

        for (int i = 0, len = params.size(); i < len; i++) {
            ps.setObject(i + 1, convertDataTypeToDB(params.get(i)));
        }
    }

    /**
     * 数据类型转换器
     *
     * @param params 参数
     * @return 返回对应数据库的数据类型
     */
    private Object convertDataTypeToDB(Object params) {
        return params;
    }

    public void setSerializer(ObjectSerializer serializer) {
        this.serializer = serializer;
    }

    public ObjectSerializer getSerializer() {
        return this.serializer;
    }

    public RepositorySupportEnum getScheme() {
        return RepositorySupportEnum.DB;
    }

    /**
     * 序列化对象
     */
    public byte[] serialize(Object obj) {
        return getSerializer().serialize(obj);
    }

    @Override
    public String componentName() {
        return "DB事务协调储存库";
    }


}
