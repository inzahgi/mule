package net.lulihu.mule.tccTransaction.annotation;

import java.lang.annotation.*;

/**
 * 数据库字段映射
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Documented
public @interface DbField {

    /**
     * 对应数据库表字段名称
     *
     * @return 字段名称
     */
    String value();

    /**
     * 是否对参数进行序列化
     *
     * @return true序列化 反之不序列化 默认为false
     */
    boolean serializer() default false;

}
