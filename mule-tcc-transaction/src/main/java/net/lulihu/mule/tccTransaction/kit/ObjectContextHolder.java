package net.lulihu.mule.tccTransaction.kit;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.Assert;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.ObjectKit.MethodKit;
import net.lulihu.ObjectKit.StrKit;
import net.lulihu.mule.tccTransaction.exception.NotFindSuitableObjectException;

import java.lang.reflect.Method;

/**
 * 对象上下文，用于获取参与者方法所在的目标对象
 */
@Slf4j
public class ObjectContextHolder {

    private ObjectContextHolder() {
    }

    /**
     * 获取实例
     */
    public static ObjectContextHolder getInstance() {
        return ObjectContextEnum.INSTANCE.getObjectContextHolder();
    }

    private ObjectContext objectContext;

    public synchronized void setObjectContext(ObjectContext objectContext) {
        if (this.objectContext != null)
            LogKit.warn(log, "重复注入对象上下文，之前上下文以及被替换...");
        this.objectContext = objectContext;
    }

    /**
     * 从容器中选择第一个合适的class类型
     *
     * @param target 目标对象
     * @return class类型
     */
    public Class<?> chooseFirstExists(Object target, Method method) {
        if (containsBean(target.getClass())) return target.getClass();

        String name = method.getName();
        Class<?>[] parameterTypes = method.getParameterTypes();

        Class<?>[] classes = target.getClass().getInterfaces();
        for (Class<?> aClass : classes) {
            if (containsBean(target.getClass())) {
                if (MethodKit.existSpecifiedMethod(aClass, name, parameterTypes))
                    return aClass;
            }
        }
        throw new NotFindSuitableObjectException("对象类型【{}】或者指定对象的方法【{}({} )】不存在上下文中...",
                target.getClass(), name, StrKit.listStitchingStr(type -> " " + type, parameterTypes));
    }


    /**
     * 判断bean是否存在
     *
     * @param requiredType bean类型
     * @return true存在反之不存在
     */
    public boolean containsBean(Class<?> requiredType) {
        try {
            getBean(requiredType);
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

    /**
     * 根据bean类型获取bean实例
     *
     * @param requiredType bean类型
     */
    public <T> T getBean(Class<T> requiredType) throws NotFindSuitableObjectException {
        assertObjectContext();
        try {
            return objectContext.getBean(requiredType);
        } catch (NotFindSuitableObjectException e) {
            throw new NotFindSuitableObjectException("未找到指定类型【{}】的对象可用", requiredType, e);
        }
    }

    private void assertObjectContext() {
        Assert.notNull(this.objectContext, "对象上下文为空无法获取指定对象类型,请检查是否注入...");
    }

    public interface ObjectContext {

        /**
         * 获取对象
         *
         * @param requiredType 对象类型
         * @return 对象
         */
        <T> T getBean(Class<T> requiredType) throws NotFindSuitableObjectException;

    }

    private enum ObjectContextEnum {
        INSTANCE;

        ObjectContextEnum() {
            this.objectContextHolder = new ObjectContextHolder();
        }

        private final ObjectContextHolder objectContextHolder;

        public ObjectContextHolder getObjectContextHolder() {
            return objectContextHolder;
        }
    }


}
