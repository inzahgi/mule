package net.lulihu.mule.tccTransaction.service.coordinator.db;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.dateTime.DateTimeKit;
import net.lulihu.mule.tccTransaction.constant.MuleConstant;
import net.lulihu.mule.tccTransaction.exception.RepositoryMapperException;
import net.lulihu.mule.tccTransaction.kit.TransactionLogTemplateKit;
import net.lulihu.mule.tccTransaction.model.MuleTransaction;
import net.lulihu.mule.tccTransaction.model.MuleTransactionCompensations;
import net.lulihu.mule.tccTransaction.service.TransactionCoordinatorRepositoryService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 事务存储 至 DB
 */
@Slf4j
public class DBTransactionCoordinatorRepositoryServiceImpl extends SqlStatementExecutor implements TransactionCoordinatorRepositoryService {

    @Override
    public boolean saveTransactionLog(MuleTransaction muleTransaction) {
        LogKit.debug(log, "保存事务信息:{}", muleTransaction);

//        通过反射生成sql模板，事件较慢导致后面的语句先执行
        try {
            List<Object> fieldValues = new ArrayList<>();
            // 获取插入语句模板模板
            String sqlTemplate = TransactionLogTemplateKit.insertSqlTemplate(appName, muleTransaction, fieldValues, getSerializer());
            // 执行
            int row = executeUpdate(sqlTemplate, fieldValues);
            return row != 0;
        } catch (IllegalAccessException e) {
            LogKit.error(log, "生成数据库插入语句时发生错误", e);
        } catch (SQLException e) {
            LogKit.error(log, "保存事务记录至数据库时发生错误", e);
        }
        return false;
    }

    @Override
    public boolean updateMuleTransactionLogStatus(MuleTransaction muleTransaction) {
        LogKit.debug(log, "修改事务信息状态:{}", muleTransaction);

        String sqlTemplate = TransactionLogTemplateKit.UPDATE_STATUS_TEMPLATE.accept(appName);
        String updateTime = DateTimeKit.date().toString(DateTimeKit.NORM_DATETIME_MS_PATTERN);
        try {
            int row = executeUpdate(sqlTemplate, muleTransaction.getStatus(), updateTime, muleTransaction.getTransId());
            return row != 0;
        } catch (SQLException e) {
            LogKit.error(log, "修改事务信息状态至数据库时发生错误", e);
        }
        return false;
    }

    @Override
    public boolean updateParticipant(MuleTransaction muleTransaction) {
        LogKit.debug(log, "修改事务参与者信息:{}", muleTransaction);

        String sqlTemplate = TransactionLogTemplateKit.UPDATE_PARTICIPANT_TEMPLATE.accept(appName);
        String updateTime = DateTimeKit.date().toString(DateTimeKit.NORM_DATETIME_MS_PATTERN);
        // 序列化参与者信息
        byte[] serialize = getSerializer().serialize(muleTransaction.getParticipants());
        try {
            int row = executeUpdate(sqlTemplate, serialize, updateTime, muleTransaction.getTransId());
            return row != 0;
        } catch (SQLException e) {
            LogKit.error(log, "修改事务参与者信息至数据库时发生错误", e);
        }
        return false;
    }

    @Override
    public MuleTransaction getMuleTransactionById(String transId) {
        LogKit.debug(log, "根据事务id【{}】获取事务记录", transId);

        String sqlTemplate = TransactionLogTemplateKit.SELECT_TRANSACTION_BY_TRANS_ID_TEMPLATE.accept(appName);
        try {
            return executeQueryOne(MuleTransaction.class, sqlTemplate, transId);
        } catch (Exception e) {
            throw new RepositoryMapperException("根据事务id获取事务记录时发生错误", e);
        }
    }

    @Override
    public boolean delete(String transId) {
        LogKit.debug(log, "根据事务id【{}】删除事务信息...", transId);

        String sqlTemplate = TransactionLogTemplateKit.DELETE_TRANSACTION_BY_TRANS_ID_TEMPLATE.accept(appName);
        try {
            int row = executeUpdate(sqlTemplate, transId);
            return row != 0;
        } catch (SQLException e) {
            LogKit.error(log, "删除事务信息时发生错误", e);
        }
        return false;
    }

    @Override
    public List<MuleTransaction> getAllMuleTransaction() {
        LogKit.debug(log, "获取所有事务记录...");

        try {
            String sqlTemplate = TransactionLogTemplateKit.SELECT_ALL_TRANSACTION_LOG.accept(appName);
            return executeQueryList(MuleTransaction.class, sqlTemplate);
        } catch (Exception e) {
            throw new RepositoryMapperException("获取所有事务记录时发生错误", e);
        }
    }

    @Override
    public boolean getOptimisticLocks(MuleTransaction transaction, Integer recoverTimeInterval) {
        String sqlTemplate = TransactionLogTemplateKit.TRANSACTION_OPTIMISTIC_LOCK_TEMPLATE.accept(appName);
        String updateTime = DateTimeKit.date().toString(DateTimeKit.NORM_DATETIME_MS_PATTERN);
        try {
            int row = executeUpdate(sqlTemplate, updateTime, transaction.getTransId(), transaction.getVersion(), recoverTimeInterval);
            return row != 0;
        } catch (SQLException e) {
            throw new RepositoryMapperException("获取事务乐观锁时发生错误", e);
        }
    }

    @Override
    public boolean saveCompensationsLog(MuleTransactionCompensations transactionCompensations) {
        LogKit.debug(log, "保存事务补偿记录", transactionCompensations);

        String tableName = appName + MuleConstant.COMPENSATIONS_SUFFIX;

        List<Object> fieldValues = new ArrayList<>();
        try {
            String sqlTemplate = TransactionLogTemplateKit.insertSqlTemplate(tableName, transactionCompensations, fieldValues, getSerializer());
            int row = executeUpdate(sqlTemplate, fieldValues);
            return row != 0;
        } catch (IllegalAccessException e) {
            LogKit.error(log, "生成数据库插入语句时发生错误", e);
        } catch (SQLException e) {
            LogKit.error(log, "保存事务记录至数据库时发生错误", e);
        }
        return false;
    }

    @Override
    public MuleTransactionCompensations getMuleTransactionCompensationsLog(MuleTransaction transaction) {
        LogKit.debug(log, "根据事务记录获取事务补偿记录", transaction);

        String tableName = appName + MuleConstant.COMPENSATIONS_SUFFIX;
        String sqlTemplate = TransactionLogTemplateKit.SELECT_TRANSACTION_COMPENSATIONS_LOG_BY_TRANS_ID_TEMPLATE.accept(tableName);
        try {
            return executeQueryOne(MuleTransactionCompensations.class, sqlTemplate, transaction.getTransId());
        } catch (Exception e) {
            throw new RepositoryMapperException("根据事务id获取事务记录时发生错误", e);
        }
    }

    @Override
    public boolean deleteTransactionCompensationsLog(MuleTransactionCompensations transactionCompensations) {
        LogKit.debug(log, "删除事务补偿信息:{}", transactionCompensations);

        String tableName = appName + MuleConstant.COMPENSATIONS_SUFFIX;
        String sqlTemplate = TransactionLogTemplateKit.DELETE_TRANSACTION_COMPENSATIONS_LOG_BY_TRANS_ID_TEMPLATE.accept(tableName);
        try {
            int row = executeUpdate(sqlTemplate, transactionCompensations.getTransId());
            return row != 0;
        } catch (SQLException e) {
            LogKit.error(log, "删除事务补偿信息时发生错误", e);
        }
        return false;
    }

    @Override
    public boolean deleteExcessCompensationRecord(int initialDelay) {
        LogKit.debug(log, "删除多余事务补偿记录...");
        String compensationsTableName = appName + MuleConstant.COMPENSATIONS_SUFFIX;
        String sqlTemplate = TransactionLogTemplateKit.DELETE_EXCESS_COMPENSATION_RECORD.accept(compensationsTableName, appName);
        try {
            return executeUpdate(sqlTemplate, initialDelay) != 0;
        } catch (SQLException e) {
            LogKit.error(log, "删除多余事务补偿记录时发生错误", e);
        }
        return false;

    }

}
