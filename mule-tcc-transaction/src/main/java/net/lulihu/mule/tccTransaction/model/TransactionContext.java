
package net.lulihu.mule.tccTransaction.model;

import lombok.ToString;
import lombok.Data;
import net.lulihu.mule.tccTransaction.enums.MuleActionEnum;
import net.lulihu.mule.tccTransaction.enums.MuleRoleEnum;

import java.io.Serializable;

/**
 * 贯穿事务的上下文
 */
@Data
@ToString
public class TransactionContext implements Serializable {

    /**
     * 事务 Id
     */
    private String transId;

    /**
     * 事件状态 {@link  MuleActionEnum}
     */
    private int action;

    /**
     * 事务参与的角色. {@link  MuleRoleEnum}
     */
    private int role;

}
