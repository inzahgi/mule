
package net.lulihu.mule.tccTransaction.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;


/**
 * 事务参与者记录
 */
@Data
@ToString
public class MuleParticipant implements Serializable {

    /**
     * 事务id
     */
    private String transId;

    /**
     * 代理方法的目标对象全类名
     */
    private Class<?> targetClass;

    /**
     * 目标方法名称
     */
    private String targetMethod;

    /**
     * 目标方法参数
     */
    private Object[] args;

    /**
     * 目标方法参数类型
     */
    private Class<?>[] parameterTypes;

    /**
     * 确认方法名称
     */
    private String confirmMethod;

    /**
     * 取消方法名称
     */
    private String cancelMethod;

    /**
     * 当前参与者回调重试次数
     */
    private Integer retries = 0;

    /**
     * 是否参加补偿方法 true为参加反之不参加
     * <p>
     * 在本地事务处理器中 true为目标方法执行过程中发生例外不参与补偿
     */
    private Boolean participateCompensation;

}
