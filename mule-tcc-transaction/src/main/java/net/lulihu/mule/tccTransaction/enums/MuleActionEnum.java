package net.lulihu.mule.tccTransaction.enums;


import net.lulihu.mule.tccTransaction.exception.NotFindSuitableObjectException;

import java.util.Arrays;

/**
 * 事件状态枚举.
 */
public enum MuleActionEnum {

    /**
     * TRYING 事件
     */
    TRYING(0, "执行try"),

    /**
     * TRYING 执行之后事件
     */
    AFTER_TRYING(1, "try执行之后"),

    /**
     * Confirming 事件
     */
    CONFIRMING(2, "confirm确认事务回调"),

    /**
     * Canceling 事件
     */
    CANCELING(3, "cancel取消事务回调"),

    /**
     * 删除事件记录
     */
    DELETE(4, "删除事务记录不执行事务回滚（作用于具有事务能力并且执行错误的方法）");

    /**
     * 事件枚举
     *
     * @param code        事件代码
     * @param description 事件描述
     */
    MuleActionEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    /**
     * 根据事件代码获取对应的事件类型
     * <p>
     * 如果无法匹配则抛出异常
     *
     * @param code 事件代码
     * @return 事件类型
     */
    public static MuleActionEnum acquire(int code) {
        for (MuleActionEnum actionEnum : MuleActionEnum.values()) {
            if (actionEnum.getCode() == code)
                return actionEnum;
        }
        throw new NotFindSuitableObjectException("错误的事件代码【{}】,目前支持的事件枚举类型为{}",
                code, Arrays.asList(MuleActionEnum.values()));
    }


    private int code;

    private String description;

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }}
