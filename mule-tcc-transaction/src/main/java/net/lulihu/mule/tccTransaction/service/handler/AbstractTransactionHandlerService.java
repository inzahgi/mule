package net.lulihu.mule.tccTransaction.service.handler;

import net.lulihu.mule.tccTransaction.service.TransactionExecutorEventService;
import net.lulihu.mule.tccTransaction.service.TransactionHandlerService;
import net.lulihu.mule.tccTransaction.service.factory.TransactionComponentFactoryService;
import net.lulihu.mule.tccTransaction.service.factory.TransactionFactoryManageEnum;


/**
 * 事件处理器抽象
 */
public abstract class AbstractTransactionHandlerService implements TransactionHandlerService {

    /**
     * 事务执行者
     */
    protected TransactionExecutorEventService transactionExecutorService;

    @Override
    public void afterInitialization() {
        TransactionComponentFactoryService factoryManage = TransactionFactoryManageEnum.INSTANCE.getTransactionFactoryManage();
        //选举执行者对象
        this.transactionExecutorService = factoryManage.electionTransactionExecutor(this);
    }

}
