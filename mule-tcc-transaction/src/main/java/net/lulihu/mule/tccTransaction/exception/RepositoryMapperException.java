package net.lulihu.mule.tccTransaction.exception;

import net.lulihu.ObjectKit.StrKit;
import net.lulihu.exception.ExceptionEnum;

/**
 * 储存库映射异常
 */
public class RepositoryMapperException extends RuntimeException {

    public RepositoryMapperException() {
        super();
    }

    public RepositoryMapperException(ExceptionEnum exceptionEnum) {
        this(exceptionEnum.getMessage());
    }

    public RepositoryMapperException(String message) {
        super(message);
    }

    public RepositoryMapperException(String message, Object... values) {
        super(StrKit.format(message, values));
    }

    public RepositoryMapperException(Throwable cause) {
        super(cause);
    }

    public RepositoryMapperException(String message, Throwable cause) {
        super(message, cause);
    }
}