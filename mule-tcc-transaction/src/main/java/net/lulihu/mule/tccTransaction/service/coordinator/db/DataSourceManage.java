package net.lulihu.mule.tccTransaction.service.coordinator.db;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import net.lulihu.Assert;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.ObjectKit.MapKit;
import net.lulihu.mule.tccTransaction.MuleTccConfig;
import net.lulihu.mule.tccTransaction.MuleTccShutdownManage;
import net.lulihu.mule.tccTransaction.exception.MuleTccException;
import net.lulihu.mule.tccTransaction.service.MuleTccShutdownService;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * 数据源管理
 */
@Slf4j
class DataSourceManage implements MuleTccShutdownService {

    // 数据源
    private DataSource dataSource;

    void setDataSource(MuleTccConfig.DbConfig config) {
        LogKit.debug(log, "开始加载Mule Tcc内部数据源...");

        try {
            this.dataSource = config.getDataSource() != null ? config.getDataSource() : initDataSource(config);
            LogKit.debug(log, "Mule Tcc内部数据源加载成功...");
        } catch (Exception e) {
            throw new MuleTccException("Mule Tcc内部数据源加载时发生例外", e);
        }

        MuleTccShutdownManage.getInstance().addComponents(this);
    }

    /**
     * 获取数据库连接
     *
     * @return 数据库连接
     * @throws SQLException 获取连接时发生例外
     */
    Connection getConnection() throws SQLException {
        Assert.notNull(this.dataSource, "数据源为空，无法获取连接...");
        return this.dataSource.getConnection();
    }

    /**
     * 初始化数据源
     *
     * @param dbConfig db配置
     * @return 数据源
     */
    private DataSource initDataSource(final MuleTccConfig.DbConfig dbConfig) {
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setPoolName("MuleTccDbPool");
        hikariDataSource.setJdbcUrl(dbConfig.getUrl());
        hikariDataSource.setDriverClassName(dbConfig.getDriverClassName());
        hikariDataSource.setUsername(dbConfig.getUsername());
        hikariDataSource.setPassword(dbConfig.getPassword());
        hikariDataSource.setMaximumPoolSize(dbConfig.getMaxActive());
        hikariDataSource.setMinimumIdle(dbConfig.getMinIdle());
        hikariDataSource.setConnectionTimeout(dbConfig.getConnectionTimeout());
        hikariDataSource.setIdleTimeout(dbConfig.getIdleTimeout());
        hikariDataSource.setMaxLifetime(dbConfig.getMaxLifetime());
        hikariDataSource.setConnectionTestQuery(dbConfig.getConnectionTestQuery());
        if (MapKit.isNotEmpty(dbConfig.getDataSourcePropertyMap())) {
            dbConfig.getDataSourcePropertyMap().forEach(hikariDataSource::addDataSourceProperty);
        }
        return hikariDataSource;
    }

    @Override
    public int order() {
        // 数据源最后关闭
        return Integer.MAX_VALUE;
    }

    @Override
    public void shutdown() {
        LogKit.debug(log, "开始关闭内部数据源...");

        if (this.dataSource instanceof HikariDataSource) {
            ((HikariDataSource) this.dataSource).close();
        }

        LogKit.debug(log, "内部数据源关闭成功...");
    }
}
