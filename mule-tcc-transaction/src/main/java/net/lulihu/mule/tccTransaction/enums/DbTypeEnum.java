package net.lulihu.mule.tccTransaction.enums;

import net.lulihu.mule.tccTransaction.exception.MuleTccException;

import java.util.Arrays;

/**
 * 事务日志db存储支持的数据库类型
 */
public enum DbTypeEnum {

    MYSQL("mysql"),
    ;

    DbTypeEnum(String dbType) {
        this.dbType = dbType;
    }

    /**
     * 根据驱动程序的全类名获取数据库类型枚举
     *
     * @param driverClassName 驱动程序的全类名
     * @return 数据库类型枚举
     */
    public static DbTypeEnum getDbTypeByDriverClassName(String driverClassName) {
        for (DbTypeEnum value : DbTypeEnum.values()) {
            if (driverClassName.contains(value.getDbType())) {
                return value;
            }
        }
        throw new MuleTccException("不支持的DB类型，目前只支持{}", Arrays.asList(DbTypeEnum.values()));
    }

    private String dbType;

    public String getDbType() {
        return dbType;
    }

    @Override
    public String toString() {
        return this.getDbType();
    }}
