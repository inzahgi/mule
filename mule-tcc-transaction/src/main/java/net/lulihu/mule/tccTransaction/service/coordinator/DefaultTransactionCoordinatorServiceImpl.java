package net.lulihu.mule.tccTransaction.service.coordinator;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.dateTime.DateTimeKit;
import net.lulihu.functional.Consumption;
import net.lulihu.functional.ConsumptionResult;
import net.lulihu.lock.ConditionLock;
import net.lulihu.mule.tccTransaction.MuleTccConfig;
import net.lulihu.mule.tccTransaction.kit.ParticipantKit;
import net.lulihu.mule.tccTransaction.kit.TransactionContextLocalKit;
import net.lulihu.mule.tccTransaction.model.MuleTransaction;
import net.lulihu.mule.tccTransaction.model.MuleTransactionCompensations;
import net.lulihu.mule.tccTransaction.model.TransactionContext;
import net.lulihu.mule.tccTransaction.service.ComponentService;
import net.lulihu.mule.tccTransaction.service.TransactionCoordinatorRepositoryService;
import net.lulihu.mule.tccTransaction.service.TransactionCoordinatorService;

import java.util.List;

/**
 * 默认的事务协调员
 */
@Slf4j
public class DefaultTransactionCoordinatorServiceImpl implements TransactionCoordinatorService, ComponentService {

    /**
     * 事务储存库
     */
    private TransactionCoordinatorRepositoryService repositoryService;

    @Override
    public void initTransactionCoordinator(TransactionCoordinatorRepositoryService repositoryService,
                                           MuleTccConfig config) throws Exception {
        LogKit.debug(log, "{} 开始初始化... ", componentName());

        this.repositoryService = repositoryService;
        this.repositoryService.initCoordinatorRepository(config);
    }

    @Override
    public void delete(MuleTransaction muleTransaction, ConditionLock conditionLock) {
        exeLockMethod(conditionLock, () -> repositoryService.delete(muleTransaction.getTransId()));
    }


    @Override
    public void deleteByContext(TransactionContext context, ConditionLock conditionLock) {
        exeLockMethod(conditionLock, () -> {
            boolean delete = repositoryService.delete(context.getTransId());
            if (!delete) { // 删除失败，保存至补偿记录中，由事务恢复程序进行补偿
                saveCompensationsLog(context);
            }
        });
    }


    @Override
    public void save(MuleTransaction muleTransaction, ConditionLock conditionLock) {
        exeLockMethod(conditionLock, () -> repositoryService.saveTransactionLog(muleTransaction));
    }

    @Override
    public void saveCompensationsLog(MuleTransactionCompensations transactionCompensations, ConditionLock conditionLock) {
        exeLockMethod(conditionLock, () -> repositoryService.saveCompensationsLog(transactionCompensations));
    }


    @Override
    public void updateParticipant(MuleTransaction muleTransaction, ConditionLock conditionLock) {
        exeLockMethod(conditionLock, () -> repositoryService.updateParticipant(muleTransaction));
    }

    @Override
    public void updateStatus(MuleTransaction muleTransaction, ConditionLock conditionLock) {
        exeLockMethod(conditionLock, () -> repositoryService.updateMuleTransactionLogStatus(muleTransaction));
    }

    @Override
    public void cancel(MuleTransaction muleTransaction, TransactionContext transactionContext, ConditionLock conditionLock) {
        exeLockMethod(conditionLock, () -> exeParticipantMethod(false, muleTransaction, transactionContext));
    }

    @Override
    public void confirm(MuleTransaction muleTransaction, TransactionContext transactionContext, ConditionLock conditionLock) {
        exeLockMethod(conditionLock, () -> exeParticipantMethod(true, muleTransaction, transactionContext));
    }

    @Override
    public MuleTransaction getMuleTransactionById(String transId, ConditionLock conditionLock) {
        return exeLockMethod(conditionLock, () -> repositoryService.getMuleTransactionById(transId));
    }

    @Override
    public void rpcConfirm(TransactionContext context, ConditionLock conditionLock) {
        exeLockMethod(conditionLock, () -> exeRpcParticipantMethod(true, context));
    }

    @Override
    public void rpcCancel(TransactionContext context, ConditionLock conditionLock) {
        exeLockMethod(conditionLock, () -> exeRpcParticipantMethod(false, context));
    }

    @Override
    public List<MuleTransaction> getAllMuleTransaction(ConditionLock conditionLock) {
        return exeLockMethod(conditionLock, () -> repositoryService.getAllMuleTransaction());
    }

    @Override
    public boolean getOptimisticLocks(MuleTransaction transaction, Integer recoverTimeInterval, ConditionLock conditionLock) {
        return exeLockMethod(conditionLock, () -> repositoryService.getOptimisticLocks(transaction, recoverTimeInterval));
    }

    @Override
    public MuleTransactionCompensations getMuleTransactionCompensationsLog(MuleTransaction transaction, ConditionLock conditionLock) {
        return exeLockMethod(conditionLock, () -> repositoryService.getMuleTransactionCompensationsLog(transaction));
    }

    @Override
    public void deleteTransactionCompensationsLog(MuleTransactionCompensations transactionCompensations, ConditionLock conditionLock) {
        exeLockMethod(conditionLock, () -> repositoryService.deleteTransactionCompensationsLog(transactionCompensations));
    }

    @Override
    public void deleteExcessCompensationRecord(int initialDelay, ConditionLock conditionLock) {
        exeLockMethod(conditionLock, () -> repositoryService.deleteExcessCompensationRecord(initialDelay));

    }

    /**
     * 执行参与者方法
     *
     * @param status             事务状态 true执行确认方法，false执行取消方法
     * @param muleTransaction    事务记录
     * @param transactionContext 事务上下文
     */
    private void exeParticipantMethod(boolean status, MuleTransaction muleTransaction, TransactionContext transactionContext) {
        // 获取乐观锁
        boolean success = repositoryService.getOptimisticLocks(muleTransaction, 0);
        if (success) {
            if (status) LogKit.debug(log, "执行事务确认:\n {} \n {}", muleTransaction, transactionContext);
            else LogKit.debug(log, "执行事务补偿:\n {} \n {}", muleTransaction, transactionContext);

            // 将事务上下文保存到当前缓存中，因为协调器为异步进行；如果涉及rpc调用，则会自动从当前线程获取事物上下文
            TransactionContextLocalKit.set(transactionContext);
            try {
                ParticipantKit.exeParticipantMethod(this, status, null, muleTransaction);
            } catch (Exception e) {
                LogKit.error(log, e.getMessage(), e.getCause());
            }
        }
    }

    /**
     * 执行Rpc参与者方法
     * <p>
     * 因为rpc跨服务问题，可能导致数据还未落地就执行事务确认或者取消方法导致出现异常
     * 如果事务记录不存在则保存至事务补偿表中，交由自我检查程序处理
     *
     * @param status  事务状态 true执行确认方法，false执行取消方法
     * @param context 事务上下文
     */
    private void exeRpcParticipantMethod(boolean status, TransactionContext context) {
        if (status) LogKit.debug(log, "Rpc执行事务确认:\n {} ", context);
        else LogKit.debug(log, "Rpc执行事务补偿:\n {} ", context);

        MuleTransaction muleTransaction = getMuleTransactionById(context.getTransId(), null);
        if (muleTransaction != null) {
            if (status) confirm(muleTransaction, context, null);
            else cancel(muleTransaction, context, null);
            // 保存
        } else saveCompensationsLog(context);
    }

    /**
     * 保存事务补偿记录
     *
     * @param context 事务上下文
     */
    private void saveCompensationsLog(TransactionContext context) {
        MuleTransactionCompensations transactionCompensations = new MuleTransactionCompensations();
        transactionCompensations.setTransId(context.getTransId());
        transactionCompensations.setStatus(context.getAction());
        transactionCompensations.setCreateTime(DateTimeKit.date().toString(DateTimeKit.NORM_DATETIME_MS_PATTERN));
        saveCompensationsLog(transactionCompensations, null);
    }

    /**
     * 有返回值执行锁方法
     *
     * @param conditionLock 条件锁
     * @param consumption   有返回值的执行表达式
     * @param <T>           返回值
     * @return 指定的返回结果
     */
    private <T> T exeLockMethod(ConditionLock conditionLock, ConsumptionResult<T> consumption) {
        try {
            if (conditionLock != null)
                conditionLock.getLock();

            // 执行目标方法
            return consumption.accept();
        } finally {
            if (conditionLock != null)
                conditionLock.unlock();
        }
    }

    /**
     * 无返回值执行锁方法
     *
     * @param conditionLock 条件锁
     * @param consumption   无返回值的执行表达式
     */
    private void exeLockMethod(ConditionLock conditionLock, Consumption consumption) {
        try {
            if (conditionLock != null)
                conditionLock.getLock();

            // 执行目标方法
            consumption.accept();
        } finally {
            if (conditionLock != null)
                conditionLock.unlock();
        }
    }

    @Override
    public String componentName() {
        return "默认事务协调器";
    }
}
